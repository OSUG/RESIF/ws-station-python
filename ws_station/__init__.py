# encoding: utf8
import os
from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory
from .settings import Settings
from .version import __version__
from .models.session import DBSession
from sqlalchemy import create_engine

def init_wsgi_api(config_path, reload=False, debug=False, locale='fr'):
    """ This function returns a Pyramid WSGI application.
    """
    # Configure
    pyramid_settings = {
        'pyramid.reload_all': 'true' if reload else 'false',
        'pyramid.reload_templates': 'true' if reload else 'false',
        'pyramid.debug_all': 'true' if debug else 'false',
        'pyramid.default_locale_name': locale,
        'jinja2.extensions': ['jinja2.ext.do']
    }

    with Configurator(settings=pyramid_settings) as config:

        # Template engine
        config.include('pyramid_jinja2')
        config.include('pyramid_layout')
        config.add_layout('ws_station.layout.MainLayout', 'ws_station:templates/layout.jinja2')

        # Routes
        config.add_route('query', '/query')

        config.add_route('wadl', '/application.wadl')
        config.add_route('version', '/version')
        config.add_route('health', '/health')
        config.add_route('home', '/')
        config.add_static_view('static', os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static'),  cache_max_age=3600)

        # Application
        config.scan('ws_station')

        # Load settings
        settings = Settings(config_path)

        # Database
        db_engine = create_engine(settings.database_uri)
        db_session = DBSession(db_engine, future=True)

        # Registry
        config.registry['settings'] = settings
        config.registry['db_session'] = db_session

        # Session
        config.set_session_factory(SignedCookieSessionFactory('WS-Station'))

    return config.make_wsgi_app()
