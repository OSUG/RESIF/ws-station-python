# encoding: utf8
import re
import logging
from pyramid.httpexceptions import HTTPBadRequest
from ws_station.tools import to_datetime
from sqlalchemy import select, and_, func
from ..db.network import Network
from ..db.station import Station
from ..db.channel import Channel
from ..db.unit import Latitude, Longitude
from . import Query


class QueryGet(Query):

    regex_network = re.compile('^[A-Z0-9,?*]+$', re.IGNORECASE)
    regex_station = re.compile('^[A-Z0-9,?*]+$', re.IGNORECASE)
    regex_location = re.compile('^[0-9,?*\-]+$', re.IGNORECASE)
    regex_channel = re.compile('^[A-Z0-9,?*]+$', re.IGNORECASE)

    @classmethod
    def from_request(cls, request):
        logging.debug(f"QueryGet.from_request({request})")
        query = cls(request.params)
        return query

    def __init__(self, parameters={}):
        logging.debug(f"QueryGet.__init__()")
        super().__init__(parameters)

    # PARAMETERS #######################################################################################################

    @property
    def network(self):
        value = self._get_parameter('network', ['net', ])
        if value is not None and not self.regex_network.fullmatch(value):
            raise HTTPBadRequest(f"Network is badly formatted")
        return value

    @property
    def station(self):
        value = self._get_parameter('station', ['sta', ])
        if value is not None and not self.regex_station.fullmatch(value):
            raise HTTPBadRequest(f"Station is badly formatted")
        return value

    @property
    def location(self):
        value = self._get_parameter('location', ['loc', ])
        if value is not None and not self.regex_location.fullmatch(value):
            raise HTTPBadRequest(f"Location is badly formatted")
        return value

    @property
    def channel(self):
        value = self._get_parameter('channel', ['cha', ])
        if value is not None and not self.regex_channel.fullmatch(value):
            raise HTTPBadRequest(f"Channel is badly formatted")
        return value

    @property
    def start(self):
        value = self._get_parameter('start', ['starttime', 'start_time'])
        if value is not None and not self.regex_datetime.fullmatch(value):
            raise HTTPBadRequest(f"Starttime is badly formatted")

        try:
            return to_datetime(value)
        except Exception as e:
            raise HTTPBadRequest(str(e))

    @property
    def end(self):
        value = self._get_parameter('end', ['endtime', 'end_time'])
        if value is not None and not self.regex_datetime.fullmatch(value):
            raise HTTPBadRequest(f"Starttime is badly formatted")

        try:
            return to_datetime(value)
        except Exception as e:
            raise HTTPBadRequest(str(e))

    # CHECK QUERY ######################################################################################################

    def check(self):
        logging.debug(f"QueryGet.check()")
        super().check()

        if 'net' in self._parameters and 'network' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'starttime' or its alias 'start' but not both at same time")

        if 'sta' in self._parameters and 'station' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'station' or its alias 'sta' but not both at same time")

        if 'loc' in self._parameters and 'location' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'location' or its alias 'loc' but not both at same time")

        if 'cha' in self._parameters and 'channel' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'channel' or its alias 'cha' but not both at same time")

        if self.start and self.end and self.start > self.end:
            raise HTTPBadRequest(f"Endtime must be greater than starttime")

    # EXECUTE QUERY ####################################################################################################

    def run_network(self, db_session):
        logging.debug(f"QueryGet.run_network({db_session})")

        # Build FROM part of statement
        statement = select(Network).distinct()

        # Build JOIN part of statement
        statement = statement.outerjoin(Station, Network.stations)
        statement = statement.outerjoin(Channel, Station.channels)

        # Build WHERE part of statement
        statement = self._where(statement, self.network, self.station, self.location, self.channel,
                                self.start, self.start_before, self.start_after,
                                self.end, self.end_before, self.end_after, self.include_restricted)

        # Build ORDER BY part of statement
        statement = statement.order_by(Network.network, Network.starttime)
        self.debug_statement(statement, db_session)

        networks = db_session.find_all(statement)
        return networks

    def run_station(self, db_session, network=None):
        logging.debug(f"QueryGet.run_station({db_session}, {network})")

        # Build FROM part of statement
        statement = select(Station).distinct(Network.network, Station.station, Station.starttime)

        # Build JOIN part of statement
        statement = statement.outerjoin(Network, Station.network)
        statement = statement.outerjoin(Channel, Station.channels)

        if self.latitude_min or self.latitude_max or (self.latitude and self.longitude and (self.radius_min or self.radius_max)):
            statement = statement.outerjoin(Latitude, Station.latitude)
        if self.longitude_min or self.longitude_max or (self.latitude and self.longitude and (self.radius_min or self.radius_max)):
            statement = statement.outerjoin(Longitude, Station.longitude)

        # Build WHERE part of statement
        statement = self._where(statement, network or self.network, self.station, self.location, self.channel,
                                self.start, self.start_before, self.start_after,
                                self.end, self.end_before, self.end_after, self.include_restricted,
                                self.latitude_min, self.latitude_max, self.longitude_min, self.longitude_max,
                                self.latitude, self.longitude, self.radius_min, self.radius_max)

        # Build ORDER BY part of statement
        statement = statement.order_by(Network.network, Station.station, Station.starttime)
        self.debug_statement(statement, db_session)

        stations = db_session.find_all(statement)
        return stations

    def run_channel(self, db_session, network=None, station=None):
        logging.debug(f"QueryGet.run_channel({db_session}, {network}, {station})")

        # Build FROM part of statement
        statement = select(Channel).distinct(Network.network, Station.station, Channel.location, Channel.channel, Channel.starttime)

        # Build JOIN part of statement
        statement = statement.outerjoin(Station, Channel.station)
        statement = statement.outerjoin(Network, Station.network)

        if self.latitude_min or self.latitude_max or (self.latitude and self.longitude and (self.radius_min or self.radius_max)):
            statement = statement.outerjoin(Latitude, Channel.latitude)
        if self.longitude_min or self.longitude_max or (self.latitude and self.longitude and (self.radius_min or self.radius_max)):
            statement = statement.outerjoin(Longitude, Channel.longitude)

        # Build WHERE part of statement
        statement = self._where(statement, network or self.network, station or self.station, self.location, self.channel,
                                self.start, self.start_before, self.start_after,
                                self.end, self.end_before, self.end_after, self.include_restricted,
                                self.latitude_min, self.latitude_max, self.longitude_min, self.longitude_max,
                                self.latitude, self.longitude, self.radius_min, self.radius_max)

        # Build ORDER BY part of statement
        statement = statement.order_by(Network.network, Station.station, Channel.location, Channel.channel, Channel.starttime)
        self.debug_statement(statement, db_session)

        channels = db_session.find_all(statement)
        return channels

    def _where(self, statement, network=None, station=None, location=None, channel=None,
               start=None, start_before=None, start_after=None,
               end=None, end_before=None, end_after=None, include_restricted=None,
               latitude_min=None, latitude_max=None, longitude_min=None, longitude_max=None,
               latitude=None, longitude=None, radius_min=None, radius_max=None):
        """
        Add the WHERE part to a statement
        :param statement: An initialized statement
        :param network: Filter by network code
        :param station: Filter by station code
        :param location: Filter by location code
        :param channel: Filter by location code
        :param start: Filter item starting at a date
        :param start_before: Filter item starting before a date
        :param start_after: Filter item starting after a date
        :param end: Filter item ending at a date
        :param end_before: Filter item ending before a date
        :param end_after: Filter item ending after a date
        :param include_restricted: Filter restricted items
        :param latitude_min: Filter item by latitude (min)
        :param latitude_max: Filter item by latitude (max)
        :param longitude_min: Filter item by longitude (min)
        :param longitude_max: Filter item by longitude (max)
        :param latitude: Filter item by latitude
        :param longitude: Filter item by longitude
        :param radius_min: Filter item by radius (min)
        :param radius_max: Filter item by radius (max)
        :return: Select statement
        """
        logging.debug(f"QueryGet._where({statement}, {network}, {station}, {location}, {channel}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after}, {include_restricted}, {latitude_min}, {latitude_max}, {longitude_min}, {longitude_max}, {latitude}, {longitude}, {radius_min}, {radius_max})")

        # Select the class to request regarding query level
        if self.level == 'network':
            class_level = Network
        elif self.level == 'station':
            class_level = Station
        elif self.level in ('channel', 'response'):
            class_level = Channel
        else:
            raise Exception(f"Unknown level '{self.level}'")

        # Initialize a container for WHERE conditions
        params = []

        # Build WHERE conditions
        if network and network != '*':
            params.append(Network._where_wildcard(network, Network.network))
        else:
            params.append(Network.id != 0)

        if station and station != '*':
            params.append(Station._where_wildcard(station, Station.station))

        if location and location != '*':
            params.append(Channel._where_wildcard(location, Channel.location))

        if channel and channel != '*':
            params.append(Channel._where_wildcard(channel, Channel.channel))

        if start:
            params.append(class_level.endtime >= to_datetime(start))
        if end:
            params.append(class_level.starttime <= to_datetime(end))

        if start_before:
            params.append(Channel.starttime < to_datetime(start_before))
        if start_after:
            params.append(Channel.starttime > to_datetime(start_after))
        if end_before:
            params.append(Channel.endtime < to_datetime(end_before))
        if end_after:
            params.append(Channel.endtime > to_datetime(end_after))

        if not include_restricted:
            params.append(class_level.policy == 'open')

        if latitude_min:
            params.append(Latitude.latitude >= latitude_min)
        if latitude_max:
            params.append(Latitude.latitude <= latitude_max)

        if longitude_min:
            params.append(Longitude.longitude >= longitude_min)
        if longitude_max:
            params.append(Longitude.longitude <= longitude_max)

        # Point (Latitude/Longitude) + Radius
        if latitude and longitude:
            # See: https://stackoverflow.com/questions/41804240/python-flask-translating-calculation-for-radius-filter-based-on-latitude-longi

            min_radius = radius_min or 0
            max_radius = radius_max or 180

            params.append(
                (func.degrees(
                    func.acos(
                        func.sin(func.radians(latitude)) * func.sin(func.radians(Latitude.latitude)) +
                        func.cos(func.radians(latitude)) * func.cos(func.radians(Latitude.latitude)) *
                        func.cos(func.radians(longitude - Longitude.longitude))
                    )
                )) >= min_radius
            )
            params.append(
                (func.degrees(
                    func.acos(
                        func.sin(func.radians(latitude)) * func.sin(func.radians(Latitude.latitude)) +
                        func.cos(func.radians(latitude)) * func.cos(func.radians(Latitude.latitude)) *
                        func.cos(func.radians(longitude - Longitude.longitude))
                    )
                )) <= max_radius
            )

        # Add the WHERE conditions to the SQL statement
        if params:
            if len(params) > 1:
                statement = statement.where(and_(*params))
            else:
                statement = statement.where(*params)

        # Return the updated statement
        return statement
