# encoding: utf8
import re
import logging
from pyramid.httpexceptions import HTTPBadRequest
from ws_station.tools import to_datetime
from sqlalchemy import and_, or_, func, select
from ..db.network import Network
from ..db.station import Station
from ..db.channel import Channel
from ..db.unit import Latitude, Longitude
from . import Query


class QueryPost(Query):

    regex_post_line_param = re.compile('^(?P<key>[A-Z]+)=(?P<value>[A-Z0-9.,?*-]+)$', re.IGNORECASE)
    regex_post_line_nslc = re.compile('^(?P<network>[A-Z0-9,?*]+)\s+(?P<station>[A-Z0-9,?*]+)\s+(?P<location>[0-9,?*\-]+)\s+(?P<channel>[A-Z0-9,?*]+)(\s+(?P<start>\*|([0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}(:[0-9]{2}(:[0-9]+)?)?)?))(\s+(?P<end>\*|([0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}(:[0-9]{2}(:[0-9]+)?)?)?)))?)?\s*$', re.IGNORECASE)

    @classmethod
    def from_request(cls, request):
        logging.debug(f"QueryPost.from_request({request})")

        params = {}
        rows = []

        for line in request.body.decode().replace("\r", '').split("\n"):

            match_param = cls.regex_post_line_param.fullmatch(line)
            match_nslc = cls.regex_post_line_nslc.fullmatch(line)
            if match_param:
                params[match_param.group('key')] = match_param.group('value')
            elif match_nslc:
                row = QueryPostRow(match_nslc.group('network'),
                                   match_nslc.group('station'),
                                   match_nslc.group('location'),
                                   match_nslc.group('channel'),
                                   match_nslc.group('start'),
                                   match_nslc.group('end'))
                rows.append(row)
            else:
                raise HTTPBadRequest(f"The request is badly formatted")

        query = cls(params, rows)
        return query

    def __init__(self, parameters={}, rows=[]):
        logging.debug(f"QueryPost.__init__({parameters}, {rows})")
        super().__init__(parameters)

        if 'net' in self._parameters or 'network' in self._parameters:
            raise HTTPBadRequest(f"Network can only be requested using a line with the following format '<network> <station> <location> <channel> <starttime> <endtime>'")

        if 'sta' in self._parameters or 'station' in self._parameters:
            raise HTTPBadRequest(f"Station can only be requested using a line with the following format '<network> <station> <location> <channel> <starttime> <endtime>'")

        if 'loc' in self._parameters or 'location' in self._parameters:
            raise HTTPBadRequest(f"Location can only be requested using a line with the following format '<network> <station> <location> <channel> <starttime> <endtime>'")

        if 'cha' in self._parameters or 'channel' in self._parameters:
            raise HTTPBadRequest(f"Channel can only be requested using a line with the following format '<network> <station> <location> <channel> <starttime> <endtime>'")

        if 'start' in self._parameters or 'starttime' in self._parameters:
            raise HTTPBadRequest(f"Starttime can only be requested using a line with the following format '<network> <station> <location> <channel> <starttime> <endtime>'")

        if 'end' in self._parameters or 'endtime' in self._parameters:
            raise HTTPBadRequest(f"Endtime can only be requested using a line with the following format '<network> <station> <location> <channel> <starttime> <endtime>'")

        self.rows = rows

    # CHECK QUERY ######################################################################################################

    def check(self):
        logging.debug(f"QueryPost.check()")
        super().check()

        if not self.rows:
            raise HTTPBadRequest("The body part must contain at least one line using the following format: '<network> <station> <location> <channel>[ <starttime>[ <endtime>]]'")

        for row in self.rows:
            row.check()

    # EXECUTE QUERY ####################################################################################################

    def run_network(self, db_session):
        logging.debug(f"QueryPost.run_network({db_session})")

        # Build FROM part of statement
        statement = select(Network).distinct()

        # Build JOIN part of statement
        statement = statement.outerjoin(Station, Network.stations)
        statement = statement.outerjoin(Channel, Station.channels)

        # Build WHERE part of statement
        statement = self._where(statement, self.rows, self.start_before, self.start_after, self.end_before, self.end_after, self.include_restricted)

        # Build ORDER BY part of statement
        statement = statement.order_by(Network.network, Network.starttime)
        self.debug_statement(statement, db_session)

        networks = db_session.find_all(statement)
        return networks

    def run_station(self, db_session, network=None):
        logging.debug(f"QueryPost.run_station({db_session}, {network})")

        # Build FROM part of statement
        statement = select(Station).distinct(Network.network, Station.station, Station.starttime)

        # Build JOIN part of statement
        statement = statement.outerjoin(Network, Station.network)
        statement = statement.outerjoin(Channel, Station.channels)

        if self.latitude_min or self.latitude_max or (self.latitude and self.longitude and (self.radius_min or self.radius_max)):
            statement = statement.outerjoin(Latitude, Station.latitude)
        if self.longitude_min or self.longitude_max or (self.latitude and self.longitude and (self.radius_min or self.radius_max)):
            statement = statement.outerjoin(Longitude, Station.longitude)

        # Build WHERE part of statement
        statement = self._where(statement, self.rows,
                                self.start_before, self.start_after, self.end_before, self.end_after, self.include_restricted,
                                self.latitude_min, self.latitude_max, self.longitude_min, self.longitude_max,
                                self.latitude, self.longitude, self.radius_min, self.radius_max)

        # Build ORDER BY part of statement
        statement = statement.order_by(Network.network, Station.station, Station.starttime)
        self.debug_statement(statement, db_session)

        stations = db_session.find_all(statement)
        return stations

    def run_channel(self, db_session, network=None, station=None):
        logging.debug(f"QueryPost.run_channel({db_session}, {network}, {station})")

        # Build FROM part of statement
        statement = select(Channel).distinct(Network.network, Station.station, Channel.location, Channel.channel, Channel.starttime)

        # Build JOIN part of statement
        statement = statement.outerjoin(Station, Channel.station)
        statement = statement.outerjoin(Network, Station.network)

        if self.latitude_min or self.latitude_max or (self.latitude and self.longitude and (self.radius_min or self.radius_max)):
            statement = statement.outerjoin(Latitude, Channel.latitude)
        if self.longitude_min or self.longitude_max or (self.latitude and self.longitude and (self.radius_min or self.radius_max)):
            statement = statement.outerjoin(Longitude, Channel.longitude)

        # Build WHERE part of statement
        statement = self._where(statement, self.rows,
                                self.start_before, self.start_after, self.end_before, self.end_after, self.include_restricted,
                                self.latitude_min, self.latitude_max, self.longitude_min, self.longitude_max,
                                self.latitude, self.longitude, self.radius_min, self.radius_max)

        # Build ORDER BY part of statement
        statement = statement.order_by(Network.network, Station.station, Channel.location, Channel.channel, Channel.starttime)
        self.debug_statement(statement, db_session)

        channels = db_session.find_all(statement)
        return channels

    def _where(self, statement, rows, start_before=None, start_after=None, end_before=None, end_after=None, include_restricted=None,
               latitude_min=None, latitude_max=None, longitude_min=None, longitude_max=None, latitude=None, longitude=None, radius_min=None, radius_max=None):
        """
        Add the WHERE part to a statement
        :param statement: An initialized statement
        :param rows: List of QueryRow objects defining the NSLC start/end parameters
        :param start_before: Filter item starting before a date
        :param start_after: Filter item starting after a date
        :param end_before: Filter item ending before a date
        :param end_after: Filter item ending after a date
        :param include_restricted: Filter restricted items
        :param latitude_min: Filter item by latitude (min)
        :param latitude_max: Filter item by latitude (max)
        :param longitude_min: Filter item by longitude (min)
        :param longitude_max: Filter item by longitude (max)
        :param latitude: Filter item by latitude
        :param longitude: Filter item by longitude
        :param radius_min: Filter item by radius (min)
        :param radius_max: Filter item by radius (max)
        :return: Select statement
        """
        logging.debug(f"QueryPost._where({statement}, {rows}, {start_before}, {start_after}, {end_before}, {end_after}, {include_restricted}, {latitude_min}, {latitude_max}, {longitude_min}, {longitude_max}, {latitude}, {longitude}, {radius_min}, {radius_max})")

        # Select the class to request regarding query level
        if self.level == 'network':
            class_level = Network
        elif self.level == 'station':
            class_level = Station
        elif self.level in ('channel', 'response'):
            class_level = Channel
        else:
            raise Exception(f"Unknown level '{self.level}'")

        # Initialize a container for WHERE conditions
        params = []
        params_rows = []

        # Build WHERE conditions for NSLC start/end
        for row in rows:
            param_row = []

            if row.network and row.network != '*':
                param_row.append(Network._where_wildcard(row.network, Network.network))
            else:
                param_row.append(Network.id != 0)

            if row.station and row.station != '*':
                param_row.append(Station._where_wildcard(row.station, Station.station))
            else:
                param_row.append(Station.id != 0)

            if row.location and row.location != '*':
                param_row.append(Channel._where_wildcard(row.location, Channel.location))

            if row.channel and row.channel != '*':
                param_row.append(Channel._where_wildcard(row.channel, Channel.channel))
            else:
                param_row.append(Channel.id != 0)

            if row.start:
                param_row.append(class_level.endtime >= to_datetime(row.start))
            if row.end:
                param_row.append(class_level.starttime <= to_datetime(row.end))

            params_rows.append(and_(*param_row))

        params.append(or_(*params_rows))

        # Build WHERE conditions for parameters
        if start_before:
            params.append(Channel.starttime < to_datetime(start_before))
        if start_after:
            params.append(Channel.starttime > to_datetime(start_after))
        if end_before:
            params.append(Channel.endtime < to_datetime(end_before))
        if end_after:
            params.append(Channel.endtime > to_datetime(end_after))

        if not include_restricted:
            params.append(class_level.policy == 'open')

        if latitude_min:
            params.append(Latitude.latitude >= latitude_min)
        if latitude_max:
            params.append(Latitude.latitude <= latitude_max)

        if longitude_min:
            params.append(Longitude.longitude >= longitude_min)
        if longitude_max:
            params.append(Longitude.longitude <= longitude_max)

        # Point (Latitude/Longitude) + Radius
        if latitude and longitude:
            # See: https://stackoverflow.com/questions/41804240/python-flask-translating-calculation-for-radius-filter-based-on-latitude-longi

            min_radius = radius_min or 0
            max_radius = radius_max or 180

            params.append(
                (func.degrees(
                    func.acos(
                        func.sin(func.radians(latitude)) * func.sin(func.radians(Latitude.latitude)) +
                        func.cos(func.radians(latitude)) * func.cos(func.radians(Latitude.latitude)) *
                        func.cos(func.radians(longitude - Longitude.longitude))
                    )
                )) >= min_radius
            )
            params.append(
                (func.degrees(
                    func.acos(
                        func.sin(func.radians(latitude)) * func.sin(func.radians(Latitude.latitude)) +
                        func.cos(func.radians(latitude)) * func.cos(func.radians(Latitude.latitude)) *
                        func.cos(func.radians(longitude - Longitude.longitude))
                    )
                )) <= max_radius
            )

        # Add the WHERE conditions to the SQL statement
        if params:
            if len(params) > 1:
                statement = statement.where(and_(*params))
            else:
                statement = statement.where(*params)

        # Return the updated statement
        return statement


class QueryPostRow(object):

    def __init__(self, network=None, station=None, location=None, channel=None, start=None, end=None):
        logging.debug(f"QueryPostRow.__init__({network}, {station}, {location}, {channel}, {start}, {end})")

        self.network = network
        self.station = station
        self.location = location
        self.channel = channel

        self._start = start
        self._end = end

    def __repr__(self):
        return f"<QueryPostRow {self.network} {self.station} {self.location} {self.channel} {self.start} {self.end}>"

    @property
    def start(self):
        if self._start and self._start != '*':
            try:
                return to_datetime(self._start)
            except Exception as e:
                raise HTTPBadRequest(str(e))
        return None

    @property
    def end(self):
        if self._end and self._end != '*':
            try:
                return to_datetime(self._end)
            except Exception as e:
                raise HTTPBadRequest(str(e))
        return None

    def check(self):
        logging.debug(f"QueryPostRow.check()")
        if self.start and self.end and self.start > self.end:
            raise HTTPBadRequest(f"Endtime must be greater than starttime")
