# encoding: utf8
import re
import logging
from pyramid.httpexceptions import HTTPBadRequest
from ws_station.tools import to_datetime


class Query(object):

    regex_datetime = re.compile('^[0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}(:[0-9]{2}(:[0-9]+)?)?)?$', re.IGNORECASE)
    regex_numeric = re.compile('^\-?[0-9]+(\.[0-9]?)?$', re.IGNORECASE)
    regex_string = re.compile('^[A-Z]+$', re.IGNORECASE)

    def __init__(self, parameters={}):
        logging.debug(f"Query.__init__{parameters})")
        self._parameters = parameters

    # PARAMETERS #######################################################################################################

    def _get_parameter(self, name, aliases=[], mandatory=False, default=None):
        # logging.debug(f"Query._get_parameter({name}, {aliases}, {mandatory}, {default})")
        if name in self._parameters:
            value = self._parameters.get(name, default)
            if value is not None and value != "":
                return value
            return None
        elif aliases:
            for alias in aliases:
                if alias in self._parameters:
                    value = self._parameters.get(alias, default)
                    if value is not None and value != "":
                        return value
                    return None

        if mandatory:
            raise HTTPBadRequest(f"A mandatory parameter is missing: {name}")
        else:
            return default

    @property
    def level(self):
        value = self._get_parameter('level', default='station')
        if value is not None and not self.regex_string.fullmatch(value):
            raise HTTPBadRequest(f"Level is badly formatted")
        if value not in ('network', 'station', 'channel', 'response'):
            raise HTTPBadRequest("The level parameter must be among 'network', 'station', 'channel', 'response'")
        return value

    @property
    def format(self):
        value = self._get_parameter('format', default='xml')
        if value is not None and not self.regex_string.fullmatch(value):
            raise HTTPBadRequest(f"Format is badly formatted")
        if value not in ('xml', 'text'):
            raise HTTPBadRequest("The format parameter must be among 'xml', 'text'")
        return value

    @property
    def nodata(self):
        value = self._get_parameter('nodata', default=204)
        if value is not None and not self.regex_numeric.fullmatch(str(value)):
            raise HTTPBadRequest(f"Nodata is badly formatted")
        if not str(value).isdigit():
            raise HTTPBadRequest("The nodata parameter must be an integer")
        return int(value)

    @property
    def include_restricted(self):
        return self._get_parameter('includerestricted', default=True)

    @property
    def match_timeseries(self):
        return self._get_parameter('matchtimeseries', default=False)

    @property
    def updated_after(self):
        value = self._get_parameter('updatedafter')
        if value is not None and not self.regex_datetime.fullmatch(value):
            raise HTTPBadRequest(f"Updatedafter is badly formatted")

        try:
            return to_datetime(value)
        except Exception as e:
            raise HTTPBadRequest(str(e))

    @property
    def start_before(self):
        value = self._get_parameter('startbefore', ['start_before', ])
        if value is not None and not self.regex_datetime.fullmatch(value):
            raise HTTPBadRequest(f"Startbefore is badly formatted")

        try:
            return to_datetime(value)
        except Exception as e:
            raise HTTPBadRequest(str(e))

    @property
    def start_after(self):
        value = self._get_parameter('startafter', ['start_after', ])
        if value is not None and not self.regex_datetime.fullmatch(value):
            raise HTTPBadRequest(f"Startafter is badly formatted")

        try:
            return to_datetime(value)
        except Exception as e:
            raise HTTPBadRequest(str(e))

    @property
    def end_before(self):
        value = self._get_parameter('endbefore', ['end_before', ])
        if value is not None and not self.regex_datetime.fullmatch(value):
            raise HTTPBadRequest(f"Endbefore is badly formatted")

        try:
            return to_datetime(value)
        except Exception as e:
            raise HTTPBadRequest(str(e))

    @property
    def end_after(self):
        value = self._get_parameter('endafter', ['end_after', ])
        if value is not None and not self.regex_datetime.fullmatch(value):
            raise HTTPBadRequest(f"Endafter is badly formatted")

        try:
            return to_datetime(value)
        except Exception as e:
            raise HTTPBadRequest(str(e))

    @property
    def latitude_min(self):
        value = self._get_parameter('minlatitude', ['minlat', ])
        if value is not None:
            if self.regex_numeric.fullmatch(value):
                raise HTTPBadRequest(f"Minlatitude is badly formatted")
            if (float(value) < -90.0 or float(value) > 90.0):
                raise HTTPBadRequest("The minimum latitude must be between -90 and 90")
        return value

    @property
    def latitude_max(self):
        value = self._get_parameter('maxlatitude', ['maxlat', ])
        if value is not None:
            if self.regex_numeric.fullmatch(value):
                raise HTTPBadRequest(f"maxlatitude is badly formatted")
            if (float(value) < -90.0 or float(value) > 90.0):
                raise HTTPBadRequest("The maximum latitude must be between -90 and 90")
        return value

    @property
    def longitude_min(self):
        value = self._get_parameter('minlongitude', ['minlon', ])
        if value is not None:
            if self.regex_numeric.fullmatch(value):
                raise HTTPBadRequest(f"Minlongitude is badly formatted")
            if (float(value) < -180.0 or float(value) > 180.0):
                raise HTTPBadRequest("The minimum longitude must be between -180 and 180")
        return value

    @property
    def longitude_max(self):
        value = self._get_parameter('maxlongitude', ['maxlon', ])
        if value is not None:
            if self.regex_numeric.fullmatch(value):
                raise HTTPBadRequest(f"Maxlongitude is badly formatted")
            if (float(value) < -180.0 or float(value) > 180.0):
                raise HTTPBadRequest("The maximum longitude must be between -180 and 180")
        return value

    @property
    def latitude(self):
        value = self._get_parameter('latitude', ['lat', ])
        if value is not None:
            if self.regex_numeric.fullmatch(value):
                raise HTTPBadRequest(f"latitude is badly formatted")
            if (float(value) < -90.0 or float(value) > 90.0):
                raise HTTPBadRequest("The latitude must be between -90 and 90")
        return value

    @property
    def longitude(self):
        value = self._get_parameter('longitude', ['lon', ])
        if value is not None:
            if self.regex_numeric.fullmatch(value):
                raise HTTPBadRequest(f"Longitude is badly formatted")
            if (float(value) < -180.0 or float(value) > 180.0):
                raise HTTPBadRequest("The longitude must be between -180 and 180")
        return value

    @property
    def radius_min(self):
        value = self._get_parameter('minradius')
        if value is not None:
            if self.regex_numeric.fullmatch(value):
                raise HTTPBadRequest(f"Minradius is badly formatted")
            if (float(value) < -180.0 or float(value) > 180.0):
                raise HTTPBadRequest("The minimum radius must be between -180 and 180")
        return value

    @property
    def radius_max(self):
        value = self._get_parameter('maxradius')
        if value is not None:
            if self.regex_numeric.fullmatch(value):
                raise HTTPBadRequest(f"Maxradius is badly formatted")
            if (float(value) < -180.0 or float(value) > 180.0):
                raise HTTPBadRequest("The maximum radius must be between -180 and 180")
        return value

    # CHECK QUERY ######################################################################################################

    def check(self):
        logging.debug(f"Query.check()")

        if 'start' in self._parameters and 'starttime' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'starttime' or its alias 'start' but not both at same time")

        if 'end' in self._parameters and 'endtime' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'endtime' or its alias 'end' but not both at same time")

        if 'lat' in self._parameters and 'latitude' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'latitude' or its alias 'lat' but not both at same time")

        if 'lon' in self._parameters and 'longitude' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'longitude' or its alias 'lon' but not both at same time")

        if 'minlon' in self._parameters and 'minlongitude' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'minlongitude' or its alias 'minlon' but not both at same time")

        if 'maxlon' in self._parameters and 'maxlongitude' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'maxlongitude' or its alias 'maxlon' but not both at same time")

        if 'minlat' in self._parameters and 'minlatitude' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'minlatitude' or its alias 'minlat' but not both at same time")

        if 'maxlat' in self._parameters and 'maxlatitude' in self._parameters:
            raise HTTPBadRequest(f"Please use either 'maxlatitude' or its alias 'maxlat' but not both at same time")

        if self.start_before and self.start_after:
            raise HTTPBadRequest(f"startbefore and startafter are mutually exclusive")

        if self.end_before and self.end_after:
            raise HTTPBadRequest(f"endbefore and endafter are mutually exclusive")

        if self.latitude_min and self.latitude_max and self.latitude_min > self.latitude_max:
            raise HTTPBadRequest(f"maxlatitude must be greater than minlatitude")

        if self.longitude_min and self.longitude_max and self.longitude_min > self.longitude_max:
            raise HTTPBadRequest(f"maxlongitude must be greater than minlongitude")

        if self.radius_min and self.radius_max and self.radius_min > self.radius_max:
            raise HTTPBadRequest(f"maxradius must be greater than minradius")

    # EXECUTE QUERY ####################################################################################################

    def run(self, db_session):
        logging.debug(f"Query.run({db_session})")

        if self.level == 'network':
            return self.run_network(db_session)
        elif self.level == 'station':
            return self.run_station(db_session)
        elif self.level in ('channel', 'response'):
            return self.run_channel(db_session)
        else:
            raise Exception(f"Unknown level '{self.level}'")

    def run_network(self, db_session):
        return NotImplemented

    def run_station(self, db_session):
        return NotImplemented

    def run_channel(self, db_session):
        return NotImplemented

    # DEBUG ############################################################################################################

    @staticmethod
    def debug_statement(statement, db_session):
        try:
            logging.debug(f"Statement: {statement.compile(db_session.get_bind(), compile_kwargs={'literal_binds': True})}")
        except Exception:
            logging.debug(f"Statement: {statement}")
