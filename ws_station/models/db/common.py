# encoding: utf8
from sqlalchemy import BigInteger, CHAR, Column, DateTime, Float, ForeignKeyConstraint, Index, PrimaryKeyConstraint, Text, UniqueConstraint, text
from sqlalchemy.orm import relationship
from .base import Base


class Comment(Base):
    __tablename__ = 'comment'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='comment_channel_id_fkey'),
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='comment_network_id_fkey'),
        ForeignKeyConstraint(['station_id'], ['station.station_id'], ondelete='SET DEFAULT', name='comment_station_id_fkey'),
        PrimaryKeyConstraint('comment_id', name='comment_pkey'),
        Index('ix_comment_005', 'network_id', 'level'),
        Index('ix_comment_006', 'station_id', 'level'),
        Index('ix_comment_007', 'channel_id', 'level'),
        Index('ix_comment_008', 'network_id', 'level'),
        {'comment': 'Table of comments'}
    )

    comment_id = Column(BigInteger, comment='Id of the comment')
    comment = Column(Text, nullable=False, server_default=text("'#'::text"), comment='comment - should follow as much as possible the RESIF nomenclature for station comment')
    level = Column(CHAR(1), nullable=False, index=True, server_default=text("'#'::bpchar"))

    network_id = Column(BigInteger, index=True, server_default=text('0'), comment='Id of the network')
    network = relationship('Network', back_populates='comments')

    station_id = Column(BigInteger, index=True, server_default=text('0'), comment='Id of the station')
    station = relationship('Station', back_populates='comments')

    channel_id = Column(BigInteger, index=True, server_default=text('0'), comment='Id of the station')
    channel = relationship('Channel', back_populates='comments')

    starttime = Column(DateTime, server_default=text("'-infinity'::timestamp without time zone"), comment='Start time for comment validity')
    endtime = Column(DateTime, server_default=text("'infinity'::timestamp without time zone"), comment='End time time for comment validity')
    source_file = Column(Text, index=True, server_default=text("''::text"))
    xml = Column(Text, server_default=text("''::text"))


class Equipment(Base):
    __tablename__ = 'equipment'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='equipment_channel_id_fkey'),
        ForeignKeyConstraint(['station_id'], ['station.station_id'], ondelete='SET DEFAULT', name='equipment_station_id_fkey'),
        PrimaryKeyConstraint('equipment_id', name='equipment_pkey'),
        Index('ix_equipment_005', 'station_id', 'level'),
        Index('ix_equipment_006', 'channel_id', 'level'),
        {'comment': 'equipment table'}
    )

    equipment_id = Column(BigInteger, server_default=text('0'))

    station_id = Column(BigInteger, index=True, server_default=text('0'), comment='A equipment is associated to a station if it is unique for all the channel')
    station = relationship('Station', back_populates='equipments')

    channel_id = Column(BigInteger, index=True, server_default=text('0'), comment='A equipment is associated to one or more channels ')
    channel = relationship('Channel', back_populates='equipments')

    internal_ressource = Column(Text, comment='This field contains a string that should serve as a unique resource identifier. Datacentre dependant - we could for instance use it to store the responsefile for the instrulent')
    type = Column(Text, index=True, comment='should be SENSOR, DATALOGGER')
    description = Column(Text, comment='The description of the equipment ... ')
    manufacturer = Column(Text)
    vendor = Column(Text)
    model = Column(Text, comment='METATYPE in RESIF nomenclature, ...')
    serialnumber = Column(Text)
    deployement_date = Column(DateTime, server_default=text("'-infinity'::timestamp without time zone"), comment='The date the instrument was initially deployed')
    removal_date = Column(DateTime, server_default=text("'infinity'::timestamp without time zone"), comment='The date the instrument has been  removed')
    calibration_date = Column(DateTime, server_default=text("'-infinity'::timestamp without time zone"), comment='The date of the last known calibration')
    level = Column(Text, index=True, server_default=text("''::text"))
    source_file = Column(Text, comment='This field contains the name file the information has been submitted by data provider')
    xml = Column(Text)


class ExternalReference(Base):
    __tablename__ = 'external_reference'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='external_reference_channel_id_fkey'),
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='external_reference_network_id_fkey'),
        ForeignKeyConstraint(['station_id'], ['station.station_id'], ondelete='SET DEFAULT', name='external_reference_station_id_fkey'),
        PrimaryKeyConstraint('external_reference_id', name='external_reference_pkey'),
        Index('ix_external_reference_005', 'network_id', 'level'),
        Index('ix_external_reference_006', 'station_id', 'level'),
        Index('ix_external_reference_007', 'channel_id', 'level'),
        {'comment': 'External Reference table'}
    )

    external_reference_id = Column(BigInteger, server_default=text('0'))

    network_id = Column(BigInteger, index=True, server_default=text('0'))
    network = relationship('Network', back_populates='external_references')

    station_id = Column(BigInteger, index=True, server_default=text('0'))
    station = relationship('Station', back_populates='external_references')

    channel_id = Column(BigInteger, index=True, server_default=text('0'))
    channel = relationship('Channel', back_populates='external_references')

    level = Column(CHAR(1), index=True, server_default=text('NULL::bpchar'))
    url = Column(Text)
    description = Column(Text)
    source_file = Column(Text, index=True)
    xml = Column(Text, server_default=text("''::text"))


class Identifier(Base):
    __tablename__ = 'identifier'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='identifier_channel_id_fkey'),
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='identifier_network_id_fkey'),
        ForeignKeyConstraint(['station_id'], ['station.station_id'], ondelete='SET DEFAULT', name='identifier_station_id_fkey'),
        PrimaryKeyConstraint('identifier_id', name='identifier_pkey'),
        Index('ix_identifier_004', 'network_id', 'level'),
        Index('ix_identifier_005', 'station_id', 'level'),
        Index('ix_identifier_006', 'channel_id', 'level'),
        {'comment': 'identifier table'}
    )

    identifier_id = Column(BigInteger, server_default=text('0'))

    network_id = Column(BigInteger, index=True, server_default=text('0'), comment='The id of the network the identifier is linked')
    network = relationship('Network', back_populates='identifiers')

    station_id = Column(BigInteger, index=True, server_default=text('0'), comment='The id of the station the identifier is linked')
    station = relationship('Station', back_populates='identifiers')

    channel_id = Column(BigInteger, index=True, server_default=text('0'), comment='The id of the channel the identifier is linked')
    channel = relationship('Channel', back_populates='identifiers')

    level = Column(CHAR(1), server_default=text('NULL::bpchar'))
    type = Column(Text, comment='type of the identifier : DOI, URI, INTERNAL, ....')
    identifier = Column(Text, server_default=text("''::text"), comment='identifier value')
    source_file = Column(Text, index=True, server_default=text('0'))
    xml = Column(Text, server_default=text("''::text"))


class Waterlevel(Base):
    __tablename__ = 'waterlevel'
    __table_args__ = (
        PrimaryKeyConstraint('waterlevel_id', name='waterlevel_pkey'),
        UniqueConstraint('waterlevel', name='waterlevel_waterlevel_key')
    )

    waterlevel_id = Column(BigInteger, server_default=text('0'))
    source_file = Column(Text, nullable=False, server_default=text("''::text"))
    waterlevel = Column(Float(53))
    level = Column(CHAR(1), server_default=text('NULL::bpchar'))
    xml = Column(Text)

    # station = relationship('Station', back_populates='waterlevel')
    # channel = relationship('Channel', back_populates='waterlevel')