# encoding: utf8
from sqlalchemy import BigInteger, Boolean, Column, DateTime, Enum, Float, ForeignKeyConstraint, Index, Integer, Numeric, PrimaryKeyConstraint, String, Table, Text, text
from sqlalchemy.orm import relationship
from .base import Base, metadata


t_assembleddata = Table(
    'assembleddata', metadata,
    Column('name', Text),
    Column('event_date', DateTime),
    Column('latitude', Numeric(8, 6)),
    Column('longitude', Numeric(9, 6)),
    Column('depth', Numeric(9, 2)),
    Column('magnitude', Numeric(4, 2)),
    Column('magtype', Text),
    Column('description', Text),
    Column('agency', Text),
    Column('pga', Float(53)),
    Column('collector', Text),
    Column('text_file', Text),
    Column('archive_file', Text),
    Column('size', BigInteger)
)


class TransactionIntegration(Base):
    __tablename__ = 'transaction_integration'
    __table_args__ = (
        PrimaryKeyConstraint('transaction_id', name='transaction_integration_pkey'),
        {'comment': 'table of transactions (data or metadata) submitted by data '
                'providers (A nodes). Only successful transaction are recorded'}
    )

    transaction_id = Column(BigInteger, server_default=text('0'))
    code = Column(Text, nullable=False, index=True, server_default=text("''::text"), comment='transaction code. unique, as given by ResifDataTransfert.py')
    type = Column(Text, nullable=False, server_default=text("'seismic_metadata_seed'::text"), comment='seismic_data,seismic_metadata')
    source_file = Column(Text, nullable=False, index=True, server_default=text("''::text"), comment='name of the source file; should be permanent, used as a key to replace/remove metadata for a station')
    update = Column(DateTime, index=True, comment='date of the integration')
    comment = Column(Text, comment='comment about transaction')


class Rall(Base):
    __tablename__ = 'rall'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='rall_channel_id_key'),
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='rall_network_id_key'),
        ForeignKeyConstraint(['station_id'], ['station.station_id'], ondelete='SET DEFAULT', name='rall_station_id_key'),
        PrimaryKeyConstraint('rall_id', name='rall_pkey1'),
        Index('ix_rall_004', 'network_id', 'station_id', 'channel_id'),
        Index('ix_rall_005', 'network', 'station', 'location', 'channel'),
        Index('ix_rall_010', 'starttime', 'endtime'),
        Index('ix_rall_017', 'network', 'station', 'location', 'channel', 'starttime', 'endtime'),
        Index('ix_rall_019', 'network', 'station', 'starttime', 'endtime'),
        Index('ix_rall_020', 'network', 'station'),
        Index('ix_rall_021', 'network', 'starttime', 'endtime')
    )

    rall_id = Column(BigInteger)

    network_id = Column(BigInteger, index=True, server_default=text('0'))
    network_ = relationship('Network')

    station_id = Column(BigInteger, index=True, server_default=text('0'))
    station_ = relationship('Station')

    channel_id = Column(BigInteger, index=True, server_default=text('0'))
    channel_ = relationship('Channel')

    source_file = Column(Text, index=True)
    network = Column(Text, index=True)
    station = Column(Text, index=True)
    location = Column(String(2), index=True)
    channel = Column(String(3), index=True)
    starttime = Column(DateTime, index=True)
    endtime = Column(DateTime, index=True)
    quality = Column(String(1), index=True)
    block_size = Column(Integer)
    year = Column(Integer, index=True)
    availability = Column(Boolean, index=True, server_default=text('true'))


class Rbud(Base):
    __tablename__ = 'rbud'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='rbud_channel_id_key'),
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='rbud_network_id_key'),
        ForeignKeyConstraint(['station_id'], ['station.station_id'], ondelete='SET DEFAULT', name='rbud_station_id_key'),
        PrimaryKeyConstraint('rbud_id', name='rbud_pkey1'),
        Index('ix_rbud_004', 'network_id', 'station_id', 'channel_id'),
        Index('ix_rbud_005', 'network', 'station', 'location', 'channel'),
        Index('ix_rbud_010', 'starttime', 'endtime'),
        Index('ix_rbud_017', 'network', 'station', 'location', 'channel', 'starttime', 'endtime'),
        Index('ix_rbud_019', 'network', 'station', 'starttime', 'endtime'),
        Index('ix_rbud_020', 'network', 'station'),
        Index('ix_rbud_021', 'network', 'starttime', 'endtime')
    )

    rbud_id = Column(BigInteger)

    network_id = Column(BigInteger, index=True, server_default=text('0'))
    network_ = relationship('Network')

    station_id = Column(BigInteger, index=True, server_default=text('0'))
    station_ = relationship('Station')

    channel_id = Column(BigInteger, index=True, server_default=text('0'))
    channel_ = relationship('Channel')

    source_file = Column(Text, index=True)
    network = Column(Text, index=True)
    station = Column(Text, index=True)
    location = Column(String(2), index=True)
    channel = Column(String(3), index=True)
    starttime = Column(DateTime, index=True)
    endtime = Column(DateTime, index=True)
    quality = Column(String(1), index=True)
    block_size = Column(Integer)
    year = Column(Integer, index=True)
    availability = Column(Boolean, index=True, server_default=text('true'))


class Rph5(Base):
    __tablename__ = 'rph5'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='rph5_channel_id_fkey'),
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='rph5_network_id_fkey'),
        ForeignKeyConstraint(['station_id'], ['station.station_id'], ondelete='SET DEFAULT', name='rph5_station_id_fkey'),
        PrimaryKeyConstraint('rph5_id', name='rph5_pkey')
    )

    rph5_id = Column(BigInteger)

    network_id = Column(BigInteger, server_default=text('0'))
    network_ = relationship('Network')

    station_id = Column(BigInteger, server_default=text('0'))
    station_ = relationship('Station')

    channel_id = Column(BigInteger, server_default=text('0'))
    channel_ = relationship('Channel')

    source_file = Column(Text)
    network = Column(String(10))
    station = Column(String(10))
    location = Column(String(10), server_default=text("''::character varying"))
    channel = Column(String(10))
    starttime = Column(DateTime)
    endtime = Column(DateTime)
    iarray = Column(String(10))
    quality = Column(String(1), server_default=text("'D'::character varying"))
    updated_at = Column(DateTime)
    policy = Column(Enum('open', 'closed', name='enum_type'))
    samplerate = Column(Text)
