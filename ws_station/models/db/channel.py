# encoding: utf8
from sqlalchemy import BigInteger, CheckConstraint, Column, DateTime, Float, ForeignKeyConstraint, Index, Integer, Numeric, PrimaryKeyConstraint, Text, UniqueConstraint, text
from sqlalchemy.orm import relationship
from .base import Base
from ws_station.tools import datetime_2100


class Channel(Base):
    __tablename__ = 'channel'
    __table_args__ = (
        CheckConstraint("policy = ANY (ARRAY['open'::text, 'closed'::text])", name='channel_policy_check'),
        ForeignKeyConstraint(['azimuth_id'], ['azimuth.azimuth_id'], ondelete='SET DEFAULT', name='channel_azimuth_id_fkey'),
        ForeignKeyConstraint(['clockdrift_id'], ['clockdrift.clockdrift_id'], ondelete='SET DEFAULT', name='channel_clockdrift_id_fkey'),
        ForeignKeyConstraint(['depth_id'], ['distance.distance_id'], ondelete='SET DEFAULT', name='channel_depth_id_fkey'),
        ForeignKeyConstraint(['dip_id'], ['dip.dip_id'], ondelete='SET DEFAULT', name='channel_dip_id_fkey'),
        ForeignKeyConstraint(['elevation_id'], ['distance.distance_id'], ondelete='SET DEFAULT', name='channel_elevation_id_fkey'),
        ForeignKeyConstraint(['latitude_id'], ['latitude.latitude_id'], ondelete='SET DEFAULT', name='channel_latitude_id_fkey'),
        ForeignKeyConstraint(['longitude_id'], ['longitude.longitude_id'], ondelete='SET DEFAULT', name='channel_longitude_id_fkey'),
        ForeignKeyConstraint(['samplerate_id'], ['samplerate.samplerate_id'], ondelete='SET DEFAULT', name='channel_samplerate_id_fkey'),
        ForeignKeyConstraint(['station_id'], ['station.station_id'], ondelete='SET DEFAULT', name='channel_station_id_fkey1'),
        ForeignKeyConstraint(['unit_id'], ['unit.unit_id'], ondelete='SET DEFAULT', name='channel_unit_id_fkey'),
        ForeignKeyConstraint(['waterlevel_id'], ['waterlevel.waterlevel_id'], ondelete='SET DEFAULT', name='channel_waterlevel_id_fkey'),
        PrimaryKeyConstraint('channel_id', name='channel_pkey1'),
        Index('ix_channel_016', 'channel_id', 'channel'),
        Index('ix_channel_017', 'channel_id', 'location'),
        Index('ix_channel_018', 'channel_id', 'starttime', 'endtime'),
        Index('ix_channel_019', 'channel_id', 'azimuth_id'),
        Index('ix_channel_020', 'channel_id', 'clockdrift_id'),
        Index('ix_channel_021', 'channel_id', 'source_file'),
        Index('ix_channel_022', 'channel_id', 'station_id'),
        Index('ix_channel_023', 'channel_id', 'latitude_id'),
        Index('ix_channel_024', 'channel_id', 'longitude_id'),
        Index('ix_channel_025', 'channel_id', 'elevation_id'),
        Index('ix_channel_027', 'channel_id', 'depth_id'),
        Index('ix_channel_028', 'channel_id', 'samplerate_id'),
        Index('ix_channel_029', 'channel_id', 'dip_id'),
        {'comment': 'Channel table'}
    )

    id = Column('channel_id', BigInteger, server_default=text('0'), comment='id of the channel')
    source_file = Column(Text, nullable=False, index=True, server_default=text("''::text"), comment='the name of the file the channel has been submitted to RESIF-DC')

    station_id = Column(BigInteger, index=True, server_default=text('0'), comment='id of the station the channel belongs to')
    station = relationship('Station', back_populates='channels')

    channel = Column(Text, index=True, comment='channel code, see Appendix A SEED manual')
    location = Column(Text, index=True, comment='location code, SEED manual')
    description = Column(Text, comment='description of the channel')
    internal_reference = Column(Text, comment='internal reference, DC dependant')
    starttime = Column(DateTime, index=True, server_default=text("'-infinity'::timestamp without time zone"), comment='starting date for this channel')
    endtime = Column(DateTime, index=True, server_default=text("'-infinity'::timestamp without time zone"), comment='ending date for this channel')
    policy = Column(Text, server_default=text("'closed'::text"))
    altcode = Column(Text, comment='another name for this channel')
    histcode = Column(Text, comment='historical name')

    latitude_id = Column(BigInteger, index=True, server_default=text('0'))
    latitude = relationship('Latitude', lazy="joined")

    longitude_id = Column(BigInteger, index=True, server_default=text('0'))
    longitude = relationship('Longitude', lazy="joined")

    elevation_id = Column(BigInteger, index=True, server_default=text('0'))
    elevation = relationship('Distance', foreign_keys=[elevation_id], lazy="joined")

    waterlevel_id = Column(BigInteger, index=True, server_default=text('0'))
    waterlevel = relationship('Waterlevel', lazy="joined")

    depth_id = Column(BigInteger, index=True, server_default=text('0'))
    depth = relationship('Distance', foreign_keys=[depth_id], lazy="joined")

    azimuth_id = Column(BigInteger, index=True, server_default=text('0'))
    azimuth = relationship('Azimuth', lazy="joined")

    dip_id = Column(BigInteger, index=True, server_default=text('0'))
    dip = relationship('Dip', lazy="joined")

    samplerate_id = Column(BigInteger, index=True, server_default=text('0'))
    samplerate = relationship('Samplerate', lazy="joined")

    clockdrift_id = Column(BigInteger, index=True, server_default=text('0'), comment='ID for the A tolerance value, measured in seconds per sample, used as a threshold for time error detection in the data')
    clockdrift = relationship('Clockdrift', lazy="joined")

    unit_id = Column(Integer, server_default=text('0'), comment='ID for the calibration unit')
    unit = relationship('Unit', lazy="joined")

    station_code = Column('station', Text, index=True)

    # Single #####
    response = relationship('Response', back_populates='channel', uselist=False)
    sensitivity = relationship('Sensitivity', back_populates='channel', uselist=False, lazy="joined")

    # Multiple ###
    equipments = relationship('Equipment', back_populates='channel', lazy='selectin')
    external_references = relationship('ExternalReference', back_populates='channel', lazy='selectin')

    comments = relationship('Comment', back_populates='channel', lazy='selectin')
    identifiers = relationship('Identifier', back_populates='channel', lazy='selectin')

    type_channel_at_channel = relationship('TypeChannelAtChannel', back_populates='channel', lazy='selectin')

    # rall = relationship('Rall', back_populates='channel_')
    # rbud = relationship('Rbud', back_populates='channel_')
    # rph5 = relationship('Rph5', back_populates='channel_')

    @property
    def code(self):
        return self.channel

    @property
    def end(self):
        if self.endtime < datetime_2100():
            return self.endtime
        return None

    @property
    def first_equipment(self):
        return self.equipments[0] if self.equipments else None

    def __repr__(self):
        return f"<Channel {self.id} {self.channel}>"


class Clockdrift(Base):
    __tablename__ = 'clockdrift'
    __table_args__ = (
        ForeignKeyConstraint(['unit_id'], ['unit.unit_id'], ondelete='SET DEFAULT', name='clockdrift_unit_id_fkey'),
        PrimaryKeyConstraint('clockdrift_id', name='clockdrift_pkey'),
        UniqueConstraint('xml', name='clockdrift_xml_key'),
        {'comment': 'clockdrift table'}
    )

    clockdrift_id = Column(BigInteger)

    unit_id = Column(BigInteger, server_default=text('0'))
    unit = relationship('Unit')

    clockdrift = Column(Float, index=True)
    xml = Column(Text, server_default=text("''::text"))


class Dip(Base):
    __tablename__ = 'dip'
    __table_args__ = (
        ForeignKeyConstraint(['unit_id'], ['unit.unit_id'], ondelete='SET DEFAULT', name='dip_unit_id_fkey'),
        PrimaryKeyConstraint('dip_id', name='dip_pkey'),
        UniqueConstraint('xml', name='dip_xml_key'),
        {'comment': 'dip table'}
    )

    dip_id = Column(BigInteger)

    unit_id = Column(Integer, server_default=text('0'))
    unit = relationship('Unit')

    dip = Column(Numeric(4, 1), index=True, server_default=text('NULL::numeric'))
    minus_error = Column(Float(53))
    plus_error = Column(Float(53))
    xml = Column(Text, server_default=text("''::text"))


class Response(Base):
    __tablename__ = 'response'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='response_channel_id_fkey'),
        PrimaryKeyConstraint('response_id', name='response_pkey'),
        {'comment': 'table of global - full response - for a given channel, as it has '
                'been defined by data provider. RESIF-DC does NOT modify the RESP '
                'expressions, which is under the responsability of A Nodes'}
    )

    response_id = Column(BigInteger, server_default=text('0'), comment='id of the response')
    source_file = Column(Text, nullable=False, index=True, server_default=text("'no_file'::text"), comment='name of the file the response has been submitted by data provider')
    xml = Column(Text, comment='the content of the response, xml form between <Response> .. </Response> ')

    channel_id = Column(BigInteger, index=True, server_default=text('0'))
    channel = relationship('Channel', back_populates='response')


class Samplerate(Base):
    __tablename__ = 'samplerate'
    __table_args__ = (
        ForeignKeyConstraint(['unit_id'], ['unit.unit_id'], ondelete='SET DEFAULT', name='samplerate_unit_id_fkey'),
        PrimaryKeyConstraint('samplerate_id', name='samplerate_pkey'),
        UniqueConstraint('xml', name='samplerate_xml_key'),
        {'comment': 'samplerate table'}
    )

    samplerate_id = Column(Integer)
    xml = Column(Text, nullable=False, server_default=text("''::text"))

    unit_id = Column(Integer, server_default=text('0'))
    unit = relationship('Unit')

    samplerate = Column(Float, index=True)
    numbersamples = Column(Integer)
    numberseconds = Column(Integer)


class Sensitivity(Base):
    __tablename__ = 'sensitivity'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='sensitivity_channel_id_fkey'),
        ForeignKeyConstraint(['unit_id'], ['unit.unit_id'], ondelete='SET DEFAULT', name='sensitivity_unit_id_fkey'),
        PrimaryKeyConstraint('sensitivity_id', name='sensitivity_pkey')
    )

    sensitivity_id = Column(BigInteger, server_default=text('0'), comment='id of the sensitivity')
    sensitivity = Column(Float, index=True, server_default=text("'1'::numeric"), comment='sensitivity of the channel')
    frequency = Column(Float, index=True, server_default=text('1'), comment='frequency of the channel')

    unit_id = Column(BigInteger, index=True, server_default=text('0'))
    unit = relationship('Unit')

    unit_value = Column('unit', Text, server_default=text("''::text"))

    channel_id = Column(BigInteger, index=True, server_default=text('0'), comment='id of the channel the sensitivity is related to')
    channel = relationship('Channel', back_populates='sensitivity')

    xml = Column(Text, comment='the content of the response, xml form between <InstrumentSensitivity> .. </InstrumentSensitivity> ')
    source_file = Column(Text, index=True, server_default=text("''::text"))
    samplerate = Column(Float(53))


class TypeChannel(Base):
    __tablename__ = 'type_channel'
    __table_args__ = (
        PrimaryKeyConstraint('type_channel_id', name='type_channel_pkey'),
        UniqueConstraint('type_channel', name='type_channel_type_channel_key'),
        {'comment': 'Type of a channel'}
    )

    id = Column('type_channel_id', Integer)
    value = Column('type_channel', Text)


class TypeChannelAtChannel(Base):
    __tablename__ = 'type_channel_at_channel'
    __table_args__ = (
        ForeignKeyConstraint(['channel_id'], ['channel.channel_id'], ondelete='SET DEFAULT', name='type_channel_at_channel_channel_id_fkey'),
        ForeignKeyConstraint(['type_channel_id'], ['type_channel.type_channel_id'], ondelete='SET DEFAULT', name='type_channel_at_channel_type_channel_id_fkey'),
        PrimaryKeyConstraint('type_channel_at_channel_id', name='type_channel_at_channel_pkey')
    )

    id = Column('type_channel_at_channel_id', BigInteger, server_default=text('0'))

    type_id = Column('type_channel_id', Integer, server_default=text('0'))
    type = relationship('TypeChannel')

    channel_id = Column(Integer, server_default=text('0'))
    channel = relationship('Channel', back_populates='type_channel_at_channel')

    source_file = Column(Text, server_default=text("''::text"))
