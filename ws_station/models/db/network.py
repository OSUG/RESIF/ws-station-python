# encoding: utf8
from sqlalchemy import BigInteger, CheckConstraint, Column, DateTime, ForeignKeyConstraint, Identity, Index, Integer, PrimaryKeyConstraint, Sequence, Table, Text, UniqueConstraint, text
from sqlalchemy.orm import relationship
from .base import Base, metadata
from ws_station.tools import datetime_2100


class Network(Base):
    __tablename__ = 'networks'
    __table_args__ = (
        CheckConstraint("policy = ANY (ARRAY['open'::text, 'closed'::text, 'partial'::text])", name='networks_policy_check1'),
        PrimaryKeyConstraint('network_id', name='networks_pkey1'),
        UniqueConstraint('network', 'start_year', name='networks_network_start_year_key1'),
        Index('ix_networks_002', 'network', 'starttime', 'endtime'),
        Index('ix_networks_003', 'network', 'start_year'),
        Index('ix_networks_004', 'network', 'start_year', 'end_year'),
        Index('ix_networks_005', 'network', 'starttime'),
        Index('ix_networks_006', 'network', 'endtime'),
        {'comment': 'table of FDSN network code'}
    )

    id = Column('network_id', BigInteger, server_default=text('0'), comment='Id of the network')
    network = Column(Text, nullable=False, index=True, server_default=text("'XX'::text"), comment='code FDSN (presently 1-2 char, should be 8 chars in miniseed 3)')
    start_year = Column(Integer, nullable=False, server_default=text('0'), comment='Network starting year')
    end_year = Column(Integer, nullable=False, server_default=text('0'), comment='Network starting year')
    description = Column(Text, comment='Network description ')
    starttime = Column(DateTime, server_default=text("'-infinity'::timestamp without time zone"), comment='starttime of the network ')
    endtime = Column(DateTime, server_default=text("'-infinity'::timestamp without time zone"), comment='endtime of the network')
    policy = Column(Text, server_default=text("'closed'::text"), comment='open/closed/partial')
    altcode = Column(Text, comment='another name for the network')
    histcode = Column(Text, comment='an historical name for the network ')
    nb_station = Column(Integer, server_default=text('0'), comment='number of stations')

    stations = relationship('Station', back_populates='network')
    operators = relationship('Operator', back_populates='network', lazy='selectin')
    external_references = relationship('ExternalReference', back_populates='network', lazy='selectin')

    comments = relationship('Comment', back_populates='network', lazy='selectin')
    identifiers = relationship('Identifier', back_populates='network', lazy='selectin')

    epos_network_map = relationship('EposNetworkMap', back_populates='network')
    resif_users = relationship('ResifUsers', back_populates='network_')

    # rall = relationship('Rall', back_populates='network_')
    # rbud = relationship('Rbud', back_populates='network_')
    # rph5 = relationship('Rph5', back_populates='network_')

    @property
    def code(self):
        return self.network

    @property
    def end(self):
        if self.endtime < datetime_2100():
            return self.endtime
        return None

    def __repr__(self):
        return f"<Network {self.id} {self.network}>"


t_eida_temp_users = Table(
    'eida_temp_users', metadata,
    Column('network', Text),
    Column('start_year', Integer),
    Column('end_year', Integer),
    Column('name', Text),
    Column('network_id', BigInteger, server_default=text('0')),
    Column('expires_at', DateTime),
    ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='eida_temp_users_network_id_fkey'),
    UniqueConstraint('name', 'network_id', name='unique_privilege')
)


class EposNetworkMap(Base):
    __tablename__ = 'epos_network_map'
    __table_args__ = (
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], name='fk_network'),
        PrimaryKeyConstraint('epos_network_map_id', name='epos_network_map_pkey')
    )

    epos_network_map_id = Column(BigInteger, Identity(always=True, start=1, increment=1, minvalue=1, maxvalue=9223372036854775807, cycle=False, cache=1))
    epos_name = Column(Text)

    network_id = Column(BigInteger)
    network = relationship('Network', back_populates='epos_network_map')

    created_at = Column(DateTime(True), server_default=text('now()'))
    updated_at = Column(DateTime(True), server_default=text('now()'))


class ResifUsers(Base):
    __tablename__ = 'resif_users'
    __table_args__ = (
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='resif_users_network_id_fkey'),
        PrimaryKeyConstraint('user_id', name='aut_user_pkey'),
        UniqueConstraint('network', 'start_year', 'end_year', 'name', name='uniq_aut_user'),
        {'comment': 'Table des autorisation d acces'}
    )

    user_id = Column(Integer, Sequence('aut_user_user_id_seq'))
    network = Column(Text, nullable=False)
    start_year = Column(Integer, nullable=False, server_default=text('0'))
    end_year = Column(Integer, nullable=False, server_default=text('0'))
    name = Column(Text, nullable=False, comment='login utilise pour l authentification. Les mots de passe associes au login ne sont pas stocke dans cette table, ni dans cette base')

    network_id = Column(BigInteger, server_default=text('0'))
    network_ = relationship('Network', back_populates='resif_users')
