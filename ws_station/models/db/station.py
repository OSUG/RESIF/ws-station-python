# encoding: utf8
from sqlalchemy import BigInteger,CheckConstraint, Column, DateTime, ForeignKeyConstraint, Integer, PrimaryKeyConstraint, Text, UniqueConstraint, text
from sqlalchemy.orm import relationship
from .base import Base
from ws_station.tools import datetime_2100


class Station(Base):
    __tablename__ = 'station'
    __table_args__ = (
        CheckConstraint('endtime >= starttime', name='station_check3'),
        CheckConstraint("policy = ANY (ARRAY['open'::text, 'closed'::text])", name='station_policy_check'),
        CheckConstraint('starttime <= endtime', name='station_check2'),
        ForeignKeyConstraint(['elevation_id'], ['distance.distance_id'], ondelete='SET DEFAULT', name='station_elevation_id_fkey'),
        ForeignKeyConstraint(['geology_id'], ['geology.geology_id'], ondelete='SET DEFAULT', name='station_geology_id_fkey'),
        ForeignKeyConstraint(['latitude_id'], ['latitude.latitude_id'], ondelete='SET DEFAULT', name='station_latitude_id_fkey'),
        ForeignKeyConstraint(['longitude_id'], ['longitude.longitude_id'], ondelete='SET DEFAULT', name='station_longitude_id_fkey'),
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='station_network_id_fkey1'),
        ForeignKeyConstraint(['site_id'], ['site.site_id'], ondelete='SET DEFAULT', name='station_site_id_fkey'),
        ForeignKeyConstraint(['vault_id'], ['vault.vault_id'], ondelete='SET DEFAULT', name='station_vault_id_fkey'),
        ForeignKeyConstraint(['waterlevel_id'], ['waterlevel.waterlevel_id'], ondelete='SET DEFAULT', name='station_waterlevel_id_fkey'),
        PrimaryKeyConstraint('station_id', name='station_pkey1')
    )

    id = Column('station_id', BigInteger, server_default=text('0'))
    source_file = Column(Text, nullable=False, index=True, server_default=text("''::text"), comment='the name of the metadata file (given by a A node) the station has been send to B node. This name must be constant, used as a key, when updating the station')

    network_id = Column(BigInteger, index=True, server_default=text('0'))
    network = relationship('Network')

    latitude_id = Column(BigInteger, index=True, server_default=text('0'))
    latitude = relationship('Latitude', lazy="joined")

    longitude_id = Column(BigInteger, index=True, server_default=text('0'))
    longitude = relationship('Longitude', lazy="joined")

    elevation_id = Column(BigInteger, index=True, server_default=text('0'))
    elevation = relationship('Distance', lazy="joined")

    waterlevel_id = Column(BigInteger, index=True, server_default=text('0'))
    waterlevel = relationship('Waterlevel', lazy="joined")

    site_id = Column(Integer, index=True, server_default=text('0'))
    site = relationship('Site', back_populates='station', lazy="joined")

    vault_id = Column(Integer, index=True, server_default=text('0'))
    vault = relationship('Vault', back_populates='station', lazy="joined")

    geology_id = Column(Integer, index=True, server_default=text('0'))
    geology = relationship('Geology', back_populates='station', lazy="joined")

    station = Column(Text, index=True, comment='the FDSN code of the station')
    description = Column(Text, comment='the description')
    starttime = Column(DateTime, index=True, server_default=text("'-infinity'::timestamp without time zone"), comment='starttime for this epoch of the station')
    endtime = Column(DateTime, index=True, server_default=text("'infinity'::timestamp without time zone"), comment='endtime for this epoch of the station')
    policy = Column(Text, server_default=text("'open'::text"), comment='is the data under embargo ?. NB : in RESIF, we do not handle embargo at a station level ; date is restricted or open at the network level')
    altcode = Column(Text, comment='An alternative code for this station')
    histcode = Column(Text, comment='An historical code for this station')
    nb_channel = Column(Integer, server_default=text('0'), comment='the number of channel in this station')
    creationdate = Column(DateTime, server_default=text("'-infinity'::timestamp without time zone"), comment='the date the station was installed')
    terminationdate = Column(DateTime, server_default=text("'infinity'::timestamp without time zone"), comment='the date the station was definitively closed')

    # Multiple
    equipments = relationship('Equipment', back_populates='station', lazy='selectin')
    operators = relationship('Operator', back_populates='station', lazy='selectin')
    external_references = relationship('ExternalReference', back_populates='station', lazy='selectin')
    channels = relationship('Channel', back_populates='station')

    comments = relationship('Comment', back_populates='station', lazy='selectin')
    identifiers = relationship('Identifier', back_populates='station', lazy='selectin')

    # rall = relationship('Rall', back_populates='station_')
    # rbud = relationship('Rbud', back_populates='station_')
    # rph5 = relationship('Rph5', back_populates='station_')

    @property
    def code(self):
        return self.station

    @property
    def end(self):
        if self.endtime < datetime_2100():
            return self.endtime
        return None

    @property
    def termination(self):
        if self.terminationdate < datetime_2100():
            return self.terminationdate
        return None

    def __repr__(self):
        return f"<Station {self.id} {self.station}>"


class Geology(Base):
    __tablename__ = 'geology'
    __table_args__ = (
        PrimaryKeyConstraint('geology_id', name='geology_pkey'),
        UniqueConstraint('geology', name='geology_geology_key'),
        {'comment': 'table of geology element, relative to Geologu element of '
                'stationXML ; RESIF networks are required to conform to RESIF '
                'nomenclature to express geology'}
    )

    geology_id = Column(Integer, comment='id of the record')
    geology = Column(Text, comment='Geology information, should follow resif nomenclature')

    station = relationship('Station', back_populates='geology')


class Site(Base):
    __tablename__ = 'site'
    __table_args__ = (
        PrimaryKeyConstraint('site_id', name='site_pkey'),
        {'comment': 'site table'}
    )

    site_id = Column(BigInteger, server_default=text('0'))
    site = Column(Text, nullable=False, index=True, server_default=text("'#'::text"), comment='The commonly used name of this station, equivalent to the SEED blockette 50, field 9')
    description = Column(Text)
    source_file = Column(Text, index=True, comment='This field contains the name file the information has been submitted by data provider')
    xml = Column(Text, server_default=text("''::text"))

    station = relationship('Station', back_populates='site')


class Vault(Base):
    __tablename__ = 'vault'
    __table_args__ = (
        PrimaryKeyConstraint('vault_id', name='vault_pkey'),
        UniqueConstraint('vault', name='vault_vault_key'),
        {'comment': 'table of vault element, relative to Vault element of stationXML ; '
                'RESIF networks are required to conform to RESIF nomenclature to '
                'express vault'}
    )

    vault_id = Column(Integer, comment='id of the record')
    vault = Column(Text, comment='vault information, should follow resif nomenclature')

    station = relationship('Station', back_populates='vault')
