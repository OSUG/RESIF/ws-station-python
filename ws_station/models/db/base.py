# encoding: utf8
from sqlalchemy import select, or_
from sqlalchemy.schema import MetaData
from sqlalchemy.ext.declarative import declarative_base


class CustomBase:

    @classmethod
    def select(cls):
        """Initiate a query on this model.
        Example::
            User.select().order_by(User.username)
        """
        return select(cls)
    
    @staticmethod
    def _where_wildcard(value, property):
        if ',' in value:
            if '?' in value:
                conditions = []
                for token in value.split(','):
                    if '?' in token or '*' in token:
                        conditions.append(property.like(token.replace('?', '_').replace('*', '%')))
                    else:
                        conditions.append(property == token)
                return or_(*conditions)
            else:
                return property.in_(value.split(','))
        elif '?' in value or '*' in value:
            return property.like(value.replace('?', '_').replace('*', '%'))
        else:
            return property == value

        


# Recommended naming convention used by Alembic, as various different database
# providers will autogenerate vastly different names making migrations more
# difficult. See: http://alembic.readthedocs.org/en/latest/naming.html
NAMING_CONVENTION = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

metadata = MetaData(naming_convention=NAMING_CONVENTION)
Base = declarative_base(cls=CustomBase, metadata=metadata)
