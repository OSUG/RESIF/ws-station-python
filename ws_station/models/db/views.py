# encoding: utf8
from sqlalchemy import BigInteger, Boolean, Column, DateTime, Index, Integer, Numeric, Table, Text
from .base import metadata


# VIEWS #############################################

t_aut_user = Table(
    'aut_user', metadata,
    Column('network_id', BigInteger),
    Column('network', Text),
    Column('start_year', Integer),
    Column('end_year', Integer),
    Column('name', Text)
)

t_network_comment = Table(
    'network_comment', metadata,
    Column('network_id', BigInteger),
    Column('xml', Text)
)


t_network_operator = Table(
    'network_operator', metadata,
    Column('network_id', BigInteger),
    Column('xml', Text)
)


t_resif_nslc = Table(
    'resif_nslc', metadata,
    Column('network', Text),
    Column('station', Text),
    Column('location', Text),
    Column('channel', Text),
    Column('source_file', Text)
)

# MATERIALIZED VIEWS #############################################

t_channel_light = Table(
    'channel_light', metadata,
    Column('channel_id', BigInteger, unique=True),
    Column('network', Text),
    Column('station', Text),
    Column('location', Text),
    Column('channel', Text),
    Column('starttime', DateTime),
    Column('endtime', DateTime),
    Column('station_id', BigInteger),
    Column('network_id', BigInteger),
    Index('index_channel_light', 'channel_id', unique=True)
)

t_channel_with_record = Table(
    'channel_with_record', metadata,
    Column('network', Text),
    Column('start_year', Integer),
    Column('end_year', Integer),
    Column('station', Text),
    Column('location', Text),
    Column('channel', Text),
    Column('starttime', DateTime),
    Column('endtime', DateTime),
    Column('policy', Text),
    Index('testidx', 'network', 'station', 'location', 'channel', 'starttime', 'endtime', unique=True)
)

t_ws_common = Table(
    'ws_common', metadata,
    Column('channel_id', BigInteger, unique=True),
    Column('network_id', BigInteger, index=True),
    Column('station_id', BigInteger, index=True),
    Column('network', Text, index=True),
    Column('station', Text, index=True),
    Column('location', Text, index=True),
    Column('channel', Text, index=True),
    Column('start_year', Integer, index=True),
    Column('end_year', Integer, index=True),
    Column('policy', Text, index=True),
    Column('latitude', Numeric(8, 6), index=True),
    Column('longitude', Numeric(9, 6), index=True),
    Column('nstarttime', DateTime, index=True),
    Column('nendtime', DateTime, index=True),
    Column('sstarttime', DateTime, index=True),
    Column('sendtime', DateTime, index=True),
    Column('cstarttime', DateTime, index=True),
    Column('cendtime', DateTime, index=True),
    Column('mtc', Boolean, index=True),
    Column('mts', Boolean, index=True),
    Column('updated', DateTime, index=True),
    Index('ix_ws_001', 'nstarttime'),
    Index('ix_ws_002', 'nendtime'),
    Index('ix_ws_003', 'nstarttime', 'nendtime'),
    Index('ix_ws_004', 'sstarttime'),
    Index('ix_ws_005', 'sendtime'),
    Index('ix_ws_006', 'sstarttime', 'sendtime'),
    Index('ix_ws_007', 'cstarttime'),
    Index('ix_ws_008', 'cendtime'),
    Index('ix_ws_009', 'cstarttime', 'cendtime'),
    Index('ix_ws_010', 'latitude'),
    Index('ix_ws_011', 'longitude'),
    Index('ix_ws_012', 'longitude', 'latitude'),
    Index('ix_ws_013', 'start_year'),
    Index('ix_ws_014', 'end_year'),
    Index('ix_ws_015', 'network', 'start_year', 'end_year'),
    Index('ix_ws_016', 'mtc'),
    Index('ix_ws_017', 'policy'),
    Index('ix_ws_018', 'updated'),
    Index('ix_ws_019', 'mts'),
    Index('ix_ws_1', 'network_id'),
    Index('ix_ws_2', 'station_id'),
    Index('ix_ws_3', 'channel_id', unique=True),
    Index('ix_ws_4', 'channel'),
    Index('ix_ws_5', 'location'),
    Index('ix_ws_6', 'network'),
    Index('ix_ws_7', 'station'),
    Index('trgm_ix_ws_4', 'channel'),
    Index('trgm_ix_ws_5', 'location'),
    Index('trgm_ix_ws_6', 'network'),
    Index('trgm_ix_ws_7', 'station')
)

t_ws_channel_text = Table(
    'ws_channel_text', metadata,
    Column('channel_id', BigInteger, unique=True),
    Column('chatext', Text),
    Index('ix_ws_channel_text', 'channel_id', unique=True),
    Index('ix_ws_channel_text_1', 'channel_id', unique=True)
)


t_ws_channel_xml = Table(
    'ws_channel_xml', metadata,
    Column('channel_id', BigInteger, unique=True),
    Column('xmlchan1', Text),
    Column('xmlchan2', Text),
    Index('ix_ws_channel_xml', 'channel_id', unique=True),
    Index('ix_ws_channel_xml_1', 'channel_id', unique=True)
)

t_ws_network_text = Table(
    'ws_network_text', metadata,
    Column('channel_id', BigInteger, unique=True),
    Column('nettext', Text),
    Index('ix_ws_network_text', 'channel_id', unique=True),
    Index('ix_ws_network_text_1', 'channel_id', unique=True)
)


t_ws_network_xml = Table(
    'ws_network_xml', metadata,
    Column('channel_id', BigInteger, unique=True),
    Column('xmlnet1', Text),
    Index('ix_ws_network_xml', 'channel_id', unique=True),
    Index('ix_ws_network_xml_1', 'channel_id', unique=True)
)


t_ws_station_text = Table(
    'ws_station_text', metadata,
    Column('channel_id', BigInteger, unique=True),
    Column('statext', Text),
    Index('ix_ws_station_text', 'channel_id', unique=True),
    Index('ix_ws_station_text_1', 'channel_id', unique=True)
)


t_ws_station_xml = Table(
    'ws_station_xml', metadata,
    Column('channel_id', BigInteger, unique=True),
    Column('xmlstat1', Text),
    Column('xmlstat11', Text),
    Column('xmlstat22', Text),
    Index('ix_ws_station_xml', 'channel_id', unique=True),
    Index('ix_ws_station_xml_1', 'channel_id', unique=True)
)