# encoding: utf8
from sqlalchemy import BigInteger, CHAR, CheckConstraint, Column, Float, ForeignKeyConstraint, Index, Integer, Numeric, PrimaryKeyConstraint, Text, UniqueConstraint, text
from sqlalchemy.orm import relationship
from .base import Base


class Azimuth(Base):
    __tablename__ = 'azimuth'
    __table_args__ = (
        ForeignKeyConstraint(['unit_id'], ['unit.unit_id'], ondelete='SET DEFAULT', name='azimuth_unit_id_fkey'),
        PrimaryKeyConstraint('azimuth_id', name='azimuth_pkey'),
        UniqueConstraint('xml', name='azimuth_xml_key'),
        {'comment': 'azimuth table'}
    )

    azimuth_id = Column(BigInteger)

    unit_id = Column(Integer, server_default=text('0'))
    unit = relationship('Unit')

    azimuth = Column(Numeric(4, 1), index=True, server_default=text('NULL::numeric'))
    minus_error = Column(Float(53))
    plus_error = Column(Float(53))
    xml = Column(Text, server_default=text("''::text"))

    # channel = relationship('Channel', back_populates='azimuth')


class Distance(Base):
    __tablename__ = 'distance'
    __table_args__ = (
        ForeignKeyConstraint(['unit_id'], ['unit.unit_id'], ondelete='SET DEFAULT', name='distance_unit_id_fkey'),
        PrimaryKeyConstraint('distance_id', name='distance_pkey'),
        Index('ix_distance_005', 'distance_id', 'distance', 'level', 'type'),
        Index('ix_distance_006', 'distance', 'level', 'type'),
        {'comment': 'distance table'}
    )

    distance_id = Column(BigInteger, server_default=text('0'))
    level = Column(CHAR(1), nullable=False, index=True, server_default=text("'#'::bpchar"))
    type = Column(CHAR(1), nullable=False, index=True, server_default=text("'#'::bpchar"))

    unit_id = Column(Integer, server_default=text('0'))
    unit = relationship('Unit')

    distance = Column(Numeric(5, 1), index=True, server_default=text('0'))
    minus_error = Column(Float(53), server_default=text('0'))
    plus_error = Column(Float(53), server_default=text('0'))
    source_file = Column(Text, index=True, server_default=text("''::text"))
    xml = Column(Text, server_default=text("''::text"))

    # station = relationship('Station', back_populates='elevation')
    # channel = relationship('Channel', foreign_keys='[Channel.depth_id]', back_populates='depth')
    # channel_ = relationship('Channel', foreign_keys='[Channel.elevation_id]', back_populates='elevation')


class Latitude(Base):
    __tablename__ = 'latitude'
    __table_args__ = (
        CheckConstraint("(latitude >= ('-90'::integer)::numeric) AND (latitude <= (90)::numeric)", name='latitude_latitude_check'),
        ForeignKeyConstraint(['unit_id'], ['unit.unit_id'], ondelete='SET DEFAULT', name='latitude_unit_id_fkey'),
        PrimaryKeyConstraint('latitude_id', name='latitude_pkey'),
        Index('ix_latitude_002', 'latitude_id', 'level'),
        Index('ix_latitude_003', 'latitude_id', 'latitude', 'level'),
        {'comment': 'latitude table'}
    )

    latitude_id = Column(BigInteger, server_default=text('0'))
    latitude = Column(Numeric(8, 6), nullable=False, index=True, server_default=text('0'))
    level = Column(CHAR(1), nullable=False, server_default=text("'#'::bpchar"))
    source_file = Column(Text, nullable=False, index=True, server_default=text("''::text"))
    xml = Column(Text, nullable=False, server_default=text("''::text"))

    unit_id = Column(Integer, server_default=text('0'))
    unit = relationship('Unit')

    minus_error = Column(Float(53), server_default=text('0'))
    plus_error = Column(Float(53), server_default=text('0'))

    # station = relationship('Station', back_populates='latitude')
    # channel = relationship('Channel', back_populates='latitude')


class Longitude(Base):
    __tablename__ = 'longitude'
    __table_args__ = (
        CheckConstraint("(longitude >= ('-180'::integer)::numeric) AND (longitude <= (180)::numeric)", name='longitude_longitude_check'),
        ForeignKeyConstraint(['unit_id'], ['unit.unit_id'], ondelete='SET DEFAULT', name='longitude_unit_id_fkey'),
        PrimaryKeyConstraint('longitude_id', name='longitude_pkey'),
        Index('ix_longitude_002', 'longitude_id', 'level'),
        Index('ix_longitude_003', 'longitude_id', 'longitude', 'level'),
        {'comment': 'longitude table'}
    )

    longitude_id = Column(BigInteger, server_default=text('0'))
    longitude = Column(Numeric(9, 6), nullable=False, index=True, server_default=text('0'))
    level = Column(CHAR(1), nullable=False, server_default=text("'#'::bpchar"))
    source_file = Column(Text, nullable=False, index=True, server_default=text("''::text"))
    xml = Column(Text, nullable=False, server_default=text("''::text"))

    unit_id = Column(Integer, server_default=text('0'))
    unit = relationship('Unit')

    minus_error = Column(Float(53), server_default=text('0'))
    plus_error = Column(Float(53), server_default=text('0'))

    # station = relationship('Station', back_populates='longitude')
    # channel = relationship('Channel', back_populates='longitude')


class Unit(Base):
    __tablename__ = 'unit'
    __table_args__ = (
        PrimaryKeyConstraint('unit_id', name='unit_pkey'),
        UniqueConstraint('unit', 'description', name='unit_unit_description_key'),
        {'comment': 'unit table'}
    )

    unit_id = Column(Integer)
    unit = Column(Text, comment='is the name of the unit')
    description = Column(Text, comment='is the description of the unit')

    # azimuth = relationship('Azimuth', back_populates='unit')
    # clockdrift = relationship('Clockdrift', back_populates='unit')
    # dip = relationship('Dip', back_populates='unit')
    # distance = relationship('Distance', back_populates='unit')
    # latitude = relationship('Latitude', back_populates='unit')
    # longitude = relationship('Longitude', back_populates='unit')
    # samplerate = relationship('Samplerate', back_populates='unit')
    # channel = relationship('Channel', back_populates='unit')
    # sensitivity = relationship('Sensitivity', back_populates='unit_')
