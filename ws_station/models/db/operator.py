# encoding: utf8
from sqlalchemy import BigInteger, Column, ForeignKeyConstraint, Index, PrimaryKeyConstraint, Text, UniqueConstraint, text
from sqlalchemy.orm import relationship
from .base import Base


class Agency(Base):
    __tablename__ = 'agency'
    __table_args__ = (
        PrimaryKeyConstraint('agency_id', name='agency_pkey'),
        UniqueConstraint('agency', name='agency_agency_key'),
        {'comment': 'Normalized form of the owner table; Agency table, as it should be '
                'if life was simple. Follows RESIF-nomenclature agency table'}
    )

    agency_id = Column(BigInteger)
    agency = Column(Text, nullable=False, server_default=text("'#'::text"), comment='The name of the agency')
    acronym = Column(Text, comment='Acronym')
    website = Column(Text)
    source_file = Column(Text, server_default=text("''::text"))
    xml = Column(Text, server_default=text("''::text"))


class Operator(Base):
    __tablename__ = 'operator'
    __table_args__ = (
        ForeignKeyConstraint(['agency_id'], ['agency.agency_id'], ondelete='SET DEFAULT', name='operator_agency_id_fkey'),
        ForeignKeyConstraint(['network_id'], ['networks.network_id'], ondelete='SET DEFAULT', name='operator_network_id_fkey'),
        ForeignKeyConstraint(['station_id'], ['station.station_id'], ondelete='SET DEFAULT', name='operator_station_id_fkey'),
        PrimaryKeyConstraint('operator_id', name='operator_pkey'),
        Index('ix_operator_005', 'network_id', 'level'),
        Index('ix_operator_006', 'station_id', 'level')
    )

    operator_id = Column(BigInteger, server_default=text('0'))

    agency_id = Column(BigInteger, server_default=text('0'))
    agency = relationship('Agency')

    operator = Column(Text)

    network_id = Column(BigInteger, index=True, server_default=text('0'))
    network = relationship('Network', back_populates='operators')

    station_id = Column(BigInteger, index=True, server_default=text('0'))
    station = relationship('Station', back_populates='operators')

    level = Column(Text, index=True)
    source_file = Column(Text, index=True)
    xml = Column(Text)
