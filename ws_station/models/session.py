# encoding: utf8
from sqlalchemy.orm import Session


class DBSession(Session):

    def find_all(self, statement):
        return self.execute(statement).scalars().all()

    def find_first(self, statement):
        return self.execute(statement).scalars().first()
