# encoding: utf8
import logging
import os.path
from pathlib import Path
from decouple import AutoConfig
from pattern_singleton import Singleton
from .tools import str2bool


class Settings(metaclass=Singleton):
    PREFIX = 'WS_STATION'

    def __init__(self, config_path=None):
        logging.debug(f"Settings.__init__({config_path})")

        # Search path for .env or settings.ini file
        self._config_path = config_path or Path(__file__).parent.resolve()
        self._config = AutoConfig(self._config_path)
        logging.debug(f"Try to load settings file from '{self._config_path}'")

        # DATABASE
        self._database_host = None
        self._database_port = None
        self._database_name = None
        self._database_user = None
        self._database_pass = None

        # API
        self._api_https = None
        self._api_host = None
        self._api_port = None
        self._api_path = None

        # Versions (Optional)
        self._version = None
        self._commit = None

    # DATABASE #########################################################################################################

    @property
    def database_uri(self):
        return f"postgresql://{self.database_user}:{self.database_pass}@{self.database_host}:{self.database_port}/{self.database_name}"

    @property
    def database_host(self):
        if self._database_host is None:
            try:
                self._database_host = self._config(f'{self.PREFIX}_DATABASE_HOST')
            except Exception as e:
                logging.error(f"Please configure the database host ({self.PREFIX}_DATABASE_HOST)")
                raise e

        return self._database_host

    @property
    def database_port(self):
        if self._database_port is None:
            try:
                self._database_port = self._config(f'{self.PREFIX}_DATABASE_PORT')
            except Exception as e:
                logging.error(f"Please configure the database port ({self.PREFIX}_DATABASE_PORT)")
                raise e

        return self._database_port

    @property
    def database_name(self):
        if self._database_name is None:
            try:
                self._database_name = self._config(f'{self.PREFIX}_DATABASE_NAME')
            except Exception as e:
                logging.error(f"Please configure the database name ({self.PREFIX}_DATABASE_NAME)")
                raise e

        return self._database_name

    @property
    def database_user(self):
        if self._database_user is None:
            try:
                self._database_user = self._config(f'{self.PREFIX}_DATABASE_USER')
            except Exception as e:
                logging.error(f"Please configure the database user ({self.PREFIX}_DATABASE_USER)")
                raise e

        return self._database_user

    @property
    def database_pass(self):
        if self._database_pass is None:
            try:
                self._database_pass = self._config(f'{self.PREFIX}_DATABASE_PASS')
            except Exception as e:
                logging.error(f"Please configure the database password ({self.PREFIX}_DATABASE_PASS)")
                raise e

        return self._database_pass

    # API ##############################################################################################################

    def api_root_url(self, with_scheme=True):
        url = ""

        # Configure the scheme and the host
        if not with_scheme:
            url += f'//{self.api_host}'
        elif self.api_https:
            url += f'https://{self.api_host}'
        else:
            url += f'http://{self.api_host}'

        # Configure the port
        if self.api_port:
            url += f":{self.api_port}"

        return url

    def api_url(self, with_scheme=True):
        url = self.api_root_url(with_scheme)

        # Configure the path
        if self.api_path:
            url += self.api_path

        return url

    @property
    def api_host(self):
        if self._api_host is None:
            try:
                self._api_host = self._config(f'{self.PREFIX}_API_HOST')
                self._api_host = str(self._api_host).rstrip('/')
            except Exception as e:
                logging.error(f"Please configure the API: {self.PREFIX}_API_HOST")
                raise e

        return self._api_host

    @property
    def api_port(self):
        if self._api_port is None:
            try:
                self._api_port = self._config(f'{self.PREFIX}_API_PORT')
            except Exception as e:
                logging.warning(f"The variable {self.PREFIX}_API_PORT is not configured. Using no value by default.")
                self._api_port = None

        return self._api_port

    @property
    def api_https(self):
        if self._api_https is None:
            try:
                self._api_https = self._config(f'{self.PREFIX}_API_HTTPS')
                self._api_https = str2bool(self._api_https)
            except Exception as e:
                logging.warning(f"The variable {self.PREFIX}_API_HTTPS is not configured. Using 'false' by default.")
                self._api_https = False

        return self._api_https

    @property
    def api_path(self):
        if self._api_path is None:
            try:
                self._api_path = self._config(f'{self.PREFIX}_API_PATH')
                if not str(self._api_path).endswith('/'):
                    self._api_path = f"{self._api_path}/"
                if not str(self._api_path).startswith('/'):
                    self._api_path = f"/{self._api_path}"

            except Exception as e:
                logging.warning(f"The variable {self.PREFIX}_API_PATH is not configured. Using '/' by default.")
                self._api_path = '/'

        return self._api_path

    # VERSION (Optional) ###############################################################################################

    @property
    def commit(self):
        if self._commit is None:
            try:
                self._commit = self._config(f'{self.PREFIX}_COMMIT')

            except Exception:
                commit_file_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', 'commit.txt')
                try:
                    if os.path.exists(commit_file_path):
                        with open(commit_file_path, 'r') as commit_file:
                            self._commit = commit_file.readline()
                except Exception:
                    self._commit = None

        return self._commit

    @property
    def version(self):
        if self._version is None:
            try:
                self._version = self._config(f'{self.PREFIX}_VERSION')
            except Exception as e:
                from .version import __version__
                self._version = __version__

        return self._version
