# encoding: utf8

class InvalidParameterValueError(Exception):

    def __init__(self, parameter, value):
        message = f"Invalid value '{value}' for parameter '{parameter}'"
        super().__init__(message)
        self.code = 400


class MissingParameterError(Exception):

    def __init__(self, parameter):
        message = f"Missing parameter '{parameter}'"
        super().__init__(message)
        self.code = 400


class NoDataError(Exception):

    def __init__(self, code=204):
        super().__init__()
        self.code = code
