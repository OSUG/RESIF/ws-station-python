# encoding: utf8
from pyramid_layout.layout import layout_config


@layout_config(template='ws_station:templates/layout.jinja2')
class MainLayout(object):

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.sidebar = False

    @property
    def settings(self):
        return self.request.registry.get('settings')

    def api_url(self, with_scheme=False):
        return self.settings.api_url(with_scheme)

    def api_root_url(self, with_scheme=False):
        return self.settings.api_root_url(with_scheme)
