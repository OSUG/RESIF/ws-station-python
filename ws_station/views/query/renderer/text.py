# encoding: utf8
import logging
from pyramid.response import Response
from pyramid.httpexceptions import HTTPBadRequest
from . import QueryRenderer


class QueryRendererText(QueryRenderer):

    @classmethod
    def render(cls, results, query, settings):
        logging.debug(f"QueryRendererText.render({query})")

        # Generate output
        if query.level == 'network':
            output = cls._render_network(results)
        elif query.level == 'station':
            output = cls._render_station(results)
        elif query.level == 'channel':
            output = cls._render_channel(results)
        elif query.level == 'response':
            output = cls._render_response(results)
        else:
            raise HTTPBadRequest(f"Level '{query.level}' not supported")

        # Render the output
        return Response(body=output, content_type="text/plain", charset='utf8')

    @staticmethod
    def _render_network(results):
        logging.debug(f"QueryRendererXml._render_network()")
        output = "#Network|Description|StartTime|EndTime|TotalStations\n"

        for network in results:
            output += f"{network.network}|"
            output += f"{network.description}|"
            output += f"{network.starttime.isoformat()}|"
            output += f"{network.end.isoformat() if network.end else ''}|"
            output += f"{network.nb_station}\n"

        return output

    @staticmethod
    def _render_station(results):
        logging.debug(f"QueryRendererXml._render_station()")
        output = "#Network|Station|Latitude|Longitude|Elevation|SiteName|StartTime|EndTime\n"

        for station in results:
            output += f"{station.network.network}|"
            output += f"{station.station}|"
            output += f"{station.latitude.latitude if station.latitude and station.latitude.latitude != None else ''}|"
            output += f"{station.longitude.longitude if station.longitude and station.longitude.longitude != None else ''}|"
            output += f"{station.elevation.distance if station.elevation and station.elevation.distance != None else ''}|"
            output += f"{station.site.site if station.site and station.site.site != None else ''}|"
            output += f"{station.starttime.isoformat()}|"
            output += f"{station.end.isoformat() if station.end else ''}\n"

        return output

    @staticmethod
    def _render_channel(results):
        logging.debug(f"QueryRendererXml._render_channel()")
        output = "#Network|Station|Location|Channel|Latitude|Longitude|Elevation|Depth|Azimuth|Dip|SensorDescription|Scale|ScaleFreq|ScaleUnits|SampleRate|StartTime|EndTime\n"

        for channel in results:
            output += f"{channel.station.network.network}|"
            output += f"{channel.station.station}|"
            output += f"{channel.location}|"
            output += f"{channel.channel}|"
            output += f"{channel.latitude.latitude if channel.latitude and channel.latitude.latitude != None else ''}|"
            output += f"{channel.longitude.longitude if channel.longitude and channel.longitude.longitude != None else ''}|"
            output += f"{channel.elevation.distance if channel.elevation and channel.elevation.distance != None else ''}|"
            output += f"{channel.depth.distance if channel.depth and channel.depth.distance != None  else ''}|"
            output += f"{channel.azimuth.azimuth if channel.azimuth and channel.azimuth.azimuth != None  else ''}|"
            output += f"{channel.dip.dip if channel.dip and channel.dip.dip != None  else ''}|"
            output += f"{channel.first_equipment.description if channel.equipments else ''}|"  # SensorDescription

            if channel.sensitivity:
                output += f"{channel.sensitivity.sensitivity if channel.sensitivity.sensitivity != None else ''}|"  # Scale
                output += f"{channel.sensitivity.frequency if channel.sensitivity.frequency != None else ''}|"    # ScaleFreq
                output += f"{channel.sensitivity.unit.unit if channel.sensitivity.unit and channel.sensitivity.unit.unit != None else ''}|"         # ScaleUnits
            else:
                output += f"|||"

            output += f"{channel.samplerate.samplerate if channel.samplerate and channel.samplerate.samplerate != None else ''}|"
            output += f"{channel.starttime.isoformat()}|"
            output += f"{channel.end.isoformat() if channel.end else ''}\n"

        return output

    @staticmethod
    def _render_response(results):
        logging.debug(f"QueryRendererXml._render_response()")
        return NotImplemented
