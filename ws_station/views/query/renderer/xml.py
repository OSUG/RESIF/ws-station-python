# encoding: utf8
import re
import logging
from datetime import datetime
from pyramid.httpexceptions import HTTPBadRequest
from pyramid.renderers import render
from pyramid.response import Response
from . import QueryRenderer


class QueryRendererXml(QueryRenderer):

    @classmethod
    def render(cls, results, query, settings):
        logging.debug(f"QueryRendererXml.render({query})")

        # Generate output
        if query.level == 'network':
            networks = cls._render_network(results)
        elif query.level == 'station':
            networks = cls._render_station(results)
        elif query.level in ('channel', 'response'):
            networks = cls._render_channel(results)
        else:
            raise HTTPBadRequest(f"Level '{query.level}' not supported")

        # Build the output
        params = {
            'now': datetime.now(),
            'query': query,
            'networks': networks,
            'settings': settings,
        }
        template = f"ws_station:templates/stationxml/{query.level}.jinja2"
        output = render(template, params)
        output = re.sub(">\s+<","><", output)
        # output = output.replace("\n", '')

        # Render the output
        return Response(body=output, content_type="application/xml", charset='utf8')

    @staticmethod
    def _render_network(results):
        logging.debug(f"QueryRendererXml._render_network()")
        return results

    @staticmethod
    def _render_station(results):
        logging.debug(f"QueryRendererXml._render_station()")
        networks = {}
        for station in results:
            if station.network.id not in networks:
                network = {'network': station.network, 'stations': []}
                networks.update({station.network.id: network})
            networks[station.network.id]['stations'].append(station)
        return networks

    @staticmethod
    def _render_channel(results):
        logging.debug(f"QueryRendererXml._render_channel()")
        networks = {}
        for channel in results:
            if channel.station.network.id not in networks:
                network = {'network': channel.station.network, 'stations': {}}
                networks.update({channel.station.network.id: network})
            if channel.station.id not in networks[channel.station.network.id]['stations']:
                station = {'station': channel.station, 'channels': []}
                networks[channel.station.network.id]['stations'].update({channel.station.id: station})
            networks[channel.station.network.id]['stations'][channel.station.id]['channels'].append(channel)
        return networks
