# encoding: utf8

class QueryRenderer(object):

    @classmethod
    def render(cls, results, query, settings):
        return NotImplemented
