# encoding: utf8
from pyramid.httpexceptions import HTTPBadRequest, HTTPNoContent
from pyramid.view import view_config
from ws_station.models.query.get import QueryGet
from ws_station.models.query.post import QueryPost
from .renderer.text import QueryRendererText
from .renderer.xml import QueryRendererXml
from .. import BaseView


@view_config(route_name='query')
class QueryView(BaseView):

    @property
    def db_session(self):
        return self.request.registry.get('db_session')

    def __call__(self):

        # Initialize a Query object regarding request method
        if self.request.method == 'GET':
            query = QueryGet.from_request(self.request)
        elif self.request.method == 'POST':
            query = QueryPost.from_request(self.request)
        else:
            raise HTTPBadRequest(f"Method '{self.request.method}' not supported")

        # Check query
        query.check()

        # Execute query
        results = query.run(self.db_session)
        if not results:
            raise HTTPNoContent(f"No {query.level} found with these parameters", code=query.nodata)

        # Render output regarding query format
        if query.format == 'text':
            renderer = QueryRendererText
            return renderer.render(results, query, self.settings)
        elif query.format == 'xml':
            renderer = QueryRendererXml
            return renderer.render(results, query, self.settings)
        else:
            raise HTTPBadRequest(f"Format '{query.format}' not supported")

# OTHER IMPLEMENTATION #################################################################################################
########################################################################################################################

# @view_config(route_name='query', request_method='GET', renderer='ws_station:templates/stationxml.jinja2')
# class QueryGetXmlView(QueryGetView):
#
#     def __call__(self):
#
#         # Initialize a Query object
#         query = QueryGet.from_request(self.request)
#
#         # Build the output
#         self.request.response.content_type = 'text/xml'
#         return {'now': datetime.now(), 'query': query, 'db_session': self.db_session}
