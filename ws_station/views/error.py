# encoding: utf8
import logging
from datetime import datetime
from pyramid.view import exception_view_config
from pyramid.httpexceptions import HTTPException
from pyramid.response import Response
from . import BaseView


@exception_view_config(HTTPException, renderer='ws_station:templates/error.jinja2')
class ErrorView(BaseView):

    def __init__(self, exc, request):
        super().__init__(request)
        self.exception = exc

    def __call__(self):
        logging.debug(f"Accept: {self.request.accept}")
        if 'html' in str(self.request.accept):
            self.request.response.status = self.exception.code
            return {
                'error': self.exception,
                'request': self.request,
                'now': datetime.now(),
                'api_url': self.settings.api_url(),
                'version': self.settings.version,
            }
        else:
            output = f"Error {self.exception.code}: {self.exception.title}\n{self.exception.explanation}\n{self.exception.detail}\n\n"
            output += f"Usage details are available from {self.settings.api_url()}\n\n"
            output += f"Request:\n {self.request.url}\n\n"
            output += f"Request Submitted:\n {datetime.now()}\n\n"
            output += f"Service version:\n {self.settings.version}"
            return Response(body=output, content_type="text/plain", charset='utf8', status_int=self.exception.code)
