# encoding: utf8
from pyramid.view import view_config
from . import BaseView


@view_config(route_name='home', renderer='ws_station:templates/home.jinja2')
class HomeView(BaseView):

    def __call__(self):
        return {
            'locale': self.locale,
        }
