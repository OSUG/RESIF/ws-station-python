# encoding: utf8
from pyramid.view import view_config


@view_config(route_name='wadl', renderer='ws_station:templates/wadl.jinja2')
class WadlView(object):

    def __init__(self, request):
        self.request = request

    def __call__(self):
        self.request.response.content_type = "text/xml"
        return {}
