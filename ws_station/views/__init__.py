# encoding: utf8
from pyramid.httpexceptions import HTTPBadRequest


class BaseView(object):

    def __init__(self, request):
        self.request = request

    @property
    def session(self):
        return self.request.session

    @property
    def settings(self):
        return self.request.registry.get('settings')

    @property
    def locale(self):
        locale = self._get_parameter('locale', ['lang'])
        default = self.request.registry.get('default_locale')
        if locale:
            self.session['locale'] = locale
            return locale
        elif 'locale' in self.session:
            return self.session.get('locale', default)
        else:
            return default

    # INTERNAL METHODS #################################################################################################

    def _get_parameter(self, name, aliases=[], mandatory=False, default=None):
        if name in self.request.params:
            value = self.request.params.get(name, default)
            if value is not None and value != "":
                return value
            return None
        elif aliases:
            for alias in aliases:
                if alias in self.request.params:
                    value = self.request.params.get(alias, default)
                    if value is not None and value != "":
                        return value
                    return None

        if mandatory:
            raise HTTPBadRequest(f"A mandatory parameter is missing: {name}")
        else:
            return default
