# encoding: utf8
from pyramid.view import view_config
from sqlalchemy_utils import database_exists
from . import BaseView


@view_config(route_name='health', renderer='ws_station:templates/health.jinja2')
class HealthView(BaseView):

    def __call__(self):

        # Try to access DB
        try:
            db_available = database_exists(self.settings.database_uri)
        except Exception:
            db_available = False

        # Change return code if DB is not ready
        if not db_available:
            self.request.response.status = 503

        return {
            'locale': self.locale,
            'app_version': self.settings.version,
            'app_commit': self.settings.commit,
            'db_available': db_available,
        }
