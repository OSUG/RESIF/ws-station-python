# encoding: utf8
from pyramid.view import view_config
from ..version import __version__


@view_config(route_name='version', renderer='string')
class VersionView(object):

    def __init__(self, request):
        self.request = request

    def __call__(self):
        self.request.response.content_type = "text/plain"
        return __version__
