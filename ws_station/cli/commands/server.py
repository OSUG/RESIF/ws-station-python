# encoding: utf8
import os
import sys
import click
import logging
import traceback
from click_loglevel import LogLevel
from ws_station.cli.tools import configure_logger

# CONFIGURATION
########################################################################################################################
# Configure logger
log_format = '%(asctime)s %(levelname)s %(message)s'


# Group all sub-commands declared below
@click.group(help='Server management')
def cli():
    pass


# START WEB SERVER
########################################################################################################################

@cli.command(help='Launch the Web server')
@click.option('--host', help='The listen host', default='0.0.0.0')
@click.option('--port', help='The listen port', default=8000)
@click.option("--reload",  is_flag=True, help='Reload mode')
@click.option("--level", 'log_level', type=LogLevel(), help='Log verbosity level')
@click.option("--sql-level", 'sql_log_level', type=LogLevel(), help='SQLAlchemy log verbosity level')
@click.option("--debug",  is_flag=True, help='Show stack trace')
@click.option('--config-path', 'config_path', type=click.Path(exists=True, readable=True), help='Custom config path')
@click.option('--locale', help='The default language', default='fr')
def web(host, port, reload, log_level, sql_log_level, debug, config_path, locale):

    # Configure loggers
    configure_logger(config_path, log_level, sql_log_level)

    try:

        # Load WSGI app
        from ws_station import init_wsgi_api
        wsgi_api = init_wsgi_api(config_path, reload, debug, locale)

        # Serve the api
        from waitress import serve
        serve(wsgi_api, host=host, port=port)

        sys.exit(os.EX_OK)
    except Exception as e:
        logging.debug(e)
        if debug:
            traceback.print_exc()
        sys.exit(os.EX_SOFTWARE)
