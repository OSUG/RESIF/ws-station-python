# encoding: utf8
import sys
import logging
import coloredlogs
from pathlib import Path
from decouple import AutoConfig


def configure_logger(config_path=None, log_level=None, sql_log_level=None):

    # Try to load settings file
    if not log_level or not sql_log_level:
        config_path or Path(__file__).parent.parent.resolve()
        config = AutoConfig(config_path)

        # Determine the log level to apply
        if not log_level:
            try:
                log_level = config(f'WS_STATION_LOG_LEVEL')
            except Exception:
                log_level = logging.INFO

        if not sql_log_level:
            try:
                sql_log_level = config(f'WS_STATION_SQL_LOG_LEVEL')
            except Exception:
                sql_log_level = logging.WARNING

    # Configure the loggers
    coloredlogs.install(level=log_level, fmt='%(asctime)s %(levelname)s %(message)s', stream=sys.stdout)
    logging.getLogger('sqlalchemy').setLevel(sql_log_level)
    logging.getLogger('ws-station').setLevel(log_level)
