# encoding: utf8
import re
import arrow
import pendulum
from datetime import timezone, date, datetime
# from obspy.core import UTCDateTime

regex_temp_network = re.compile("^[XYZ0-9][A-Z0-9]?$", flags=re.IGNORECASE)


def is_temporary_network(code):
    """
    Check if a network code defines a temporary network or not
    :param code: The network code
    :return: Trye if the network is temporary, else False
    :rtype: bool
    """
    if regex_temp_network.match(code):
        return True
    else:
        return False


def value_or_default(value, default=None):
    """
    Return the value or a default value
    :param value: The value
    :param default: The default value
    :return: Return the value if not None, else the default
    """
    if value is not None:
        return value
    else:
        return default


def str2bool(value):
    """
    Return the boolean value
    :param value: The value
    :rtype: boolean
    """
    if value is None or isinstance(value, bool):
        return value
    return value.lower() in ("yes", "true", "t", "1", "on")


def to_datetime(value, tzinfo=None):
    """
    Convert the value as a datetime
    :param value: The value to convert
    :rtype: datetime
    """
    if value is None:
        return value
    # elif isinstance(value, (arrow.Arrow, UTCDateTime)):
    elif isinstance(value, arrow.Arrow):
        value = value.datetime
    elif isinstance(value, date):
        value = datetime.combine(value, datetime.min.time(), tzinfo=tzinfo)
    elif isinstance(value, str):
        if value.lower() == 'currentutcday':
            return datetime_today(tzinfo)
        else:
            value = pendulum.parse(value)
            if not tzinfo:
                return value.naive()
            return value.astimezone(tzinfo)
    return pendulum.instance(value, tzinfo)


def to_date(value):
    """
    Convert the value as a date
    :param value: The value to convert
    :rtype: date
    """
    if value is None:
        return value
    elif isinstance(value, (datetime, arrow.Arrow)):
        value = value.date()
    # elif isinstance(value, UTCDateTime):
    #     value = value.date
    elif isinstance(value, str):
        if value.lower() == 'currentutcday':
            value = date_today()
        else:
            value = pendulum.parse(value).date()
    return value


# def to_UTCDateTime(value):
#     """
#     Convert the value as a UTCDateTime
#     :param value: The value to convert
#     :rtype: UTCDateTime
#     """
#     if value is None:
#         return value
#     elif isinstance(value, (arrow.Arrow, UTCDateTime)):
#         value = value.datetime
#     elif isinstance(value, str):
#         if value.lower() == 'currentutcday':
#             value = datetime_today()
#         else:
#             value = pendulum.parse(value)
#     return UTCDateTime(value)


def date_today():
    """
    Return the date of today
    :rtype: date
    """
    return date.today()


def datetime_today(tzinfo=None):
    """
    Return the datetime of today at 00:00:00
    :rtype: datetime
    """
    if tzinfo == 'utc':
        tzinfo = timezone.utc

    return datetime.combine(date.today(), datetime.min.time(), tzinfo=tzinfo)


def datetime_now(tzinfo=None):
    """
    Return the datetime of today at current time
    :rtype: datetime
    """
    if tzinfo == 'utc':
        tzinfo = timezone.utc

    return datetime.now(tzinfo)


def datetime_2100(tzinfo=None):
    """
    Return the datetime of 2100-01-01 at 00:00:00
    :rtype: datetime
    """
    if tzinfo == 'utc':
        tzinfo = timezone.utc

    return datetime(2100, 1, 1, tzinfo=tzinfo)


date_2100 = date(2100, 1, 1)
