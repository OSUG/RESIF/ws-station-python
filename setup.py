# encoding: utf8
import os
from io import open
from setuptools import find_packages, setup

current_path = os.path.abspath(os.path.dirname(__file__))

try:
    with open(os.path.join(current_path, 'ws_station', 'version.py'), 'r') as f:
        for line in f:
            if line.startswith('__version__'):
                version = line.strip().split('=')[1].strip(' \'"')
                break
        else:
            version = '0.0.1'
except FileNotFoundError:
    version = '0.0.1'

try:
    with open(os.path.join(current_path, 'README.rst'), 'r', encoding='utf-8') as f:
        readme = f.read()
except FileNotFoundError:
    readme = ''

INSTALL_REQUIRES = [
    'click',
    'click-loglevel',

    'python-decouple',
    'pattern-singleton',

    'sqlalchemy>=1.4',
    'sqlalchemy-utils',

    'arrow',
    'pendulum',

    'pyramid',
    'pyramid_jinja2',
    'pyramid_layout',
    'waitress',
]

kwargs = {
    'name': 'ws-station',
    'version': version,
    'description': 'WS-Station',
    'long_description': readme,
    'author': 'Philippe Bollard',
    'author_email': 'dc@resif.fr',
    'maintainer': 'Résif-DC',
    'maintainer_email': 'dc@resif.fr',
    'url': 'https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-station-python',
    'license': 'GPLv3',
    'classifiers': [
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
    'install_requires': INSTALL_REQUIRES,
    'tests_require': ['coverage', 'pytest'],
    'packages': find_packages(exclude=('tests', 'tests.*')),
    'include_package_data': True,
    'entry_points': '''
        [console_scripts]
        ws-station-cli=ws_station.cli:cli
    ''',

}

setup(**kwargs)
