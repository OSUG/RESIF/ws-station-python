FROM python:3.9-slim
RUN apt-get update && apt-get upgrade -y && apt-get install -y --no-install-recommends gcc libc6-dev

RUN useradd --create-home --shell /bin/bash ws-station
WORKDIR /home/ws-station
USER ws-station

ENV VIRTUAL_ENV=/home/ws-station/.venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY requirements.txt ./
COPY setup.py ./
COPY ws_station ./ws_station
COPY .env-production ./.env

RUN pip3 install --upgrade -r requirements.txt
RUN pip3 install --upgrade -e .

ENTRYPOINT ["ws-station-cli"]
CMD ["server", "web"]
